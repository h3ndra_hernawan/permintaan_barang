    
    $('#tanggal').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $(document).ready(function () {
      
        // Footer
        $('#nik').on('click', function () {
            var nik = document.getElementById('nik');
            $('#nama').empty();
            $('#departemen').empty()
            $.ajax({
                url: "{{ url('/data/karyawan') }}",
                method: "GET",
                data: {
                    id: nik.value
                },
                success: function (res) {
                    res.forEach(element => {
                        document.getElementById('nama').value  = element.nama
                        document.getElementById('Departemen').value   = element.departemen 
                    });
                }
            });
        }) 

        // Input Data Barang Header
        var i = 0;
   
        $("#add").click(function(e){
            ++i;  
            status_detail += i;
            console.log('status' + status_detail)

            validate();
            $("#dynamicTable").append(`
                    <tr>
                        <td>
                            <div class="form-group">  
                            <input class="form-control @error('created_at') is-invalid @enderror" id="created_at" name="permintaan['`+i+`'][created_at]" type="hidden" value="{{ date('Y-m-d H:i:s') }}"placeholder="Masukan created_at"required>                   
                                <select class="form-control select form-input-select" onchange='barang(`+i+`)' onblur=validate(`+i+`) onclick=validate(`+i+`) data-fouc id="kode_barang`+ i +`"
                                    name="permintaan['`+i+`'][kode_barang]"required>
                                    <option value="" disabled selected>Pilih Master Barang</option>
                                    @foreach ($master_barang as $item)
                                    <option value="{{ $item->kode_barang }}" >{{ $item->kode_barang }} - {{ $item->nama_barang}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('kode_barang'))
                                    <span class="alert-message">{{$errors->first('kode_barang')}}</span>
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input class="form-control @error('lokasi') is-invalid @enderror" onblur=validate(`+i+`) id="lokasi`+i+`" name="permintaan['`+i+`'][lokasi]" type="text" placeholder="Lokasi" value="{{old('lokasi')}}"readonly>
                                @if($errors->has('lokasi'))
                                    <span class="alert-message">{{$errors->first('lokasi')}}</span>
                                @endif
                            </div>
                        </td> 
                        <td>
                            <div class="form-group">
                            <input class="form-control price" placeholder="Masukan Tersedia" onblur=validate(`+i+`) id="tersedia`+i+`" name="permintaan['`+i+`'][tersedia]"readonly>
                            @if($errors->has('tersedia'))
                                <span class="alert-message">{{$errors->first('tersedia')}}</span>
                            @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                            <input type="number" class="form-control kuantiti" placeholder="Masukan Kuantiti" onblur=validate(`+i+`) onclick=validate(`+i+`) oninput='qty(`+i+`)' id="kuantiti`+i+`" name="permintaan['`+i+`'][kuantiti]">
                            @if($errors->has('kuantiti'))
                                <span class="alert-message">{{$errors->first('kuantiti')}}</span>
                            @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control price" placeholder="Satuan" onblur=validate(`+i+`) id="satuan`+i+`" name="permintaan['`+i+`'][satuan]"readonly>
                            @if($errors->has('satuan'))
                                <span class="alert-message">{{$errors->first('satuan')}}</span>
                            @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control price" placeholder="Masukan Keterangan" onblur=validate(`+i+`) onclick=validate(`+i+`) id="keterangan`+i+`" name="permintaan['`+i+`'][keterangan]"required>
                            @if($errors->has('keterangan'))
                                <span class="alert-message">{{$errors->first('keterangan')}}</span>
                            @endif
                            </div>
                        </td>
                        <td><button type="button" id="remove_detail" onclick='hapusdetail()' class="btn btn-danger remove-tr" >Remove</button></td>
                    </tr>
                `);
        });
        $(document).on('click', '.remove-tr', function(){  
                $(this).parents('tr').remove();
        }); 
    });