<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PDF;
use Notification;
use App\Notifications\MyFirstNotification;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class ProjekUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        date_default_timezone_set("Asia/Jakarta") ;
        $new_date = date('dHis')."/kd/PER_BRG/".date('m').'/'.date('Y'); 
        $users = DB::table('users')->get();
        $client = DB::table('client')->get();
        // $projek_header = DB::table('projek_header')
        //     ->join('users', 'projek_header.userid', '=', 'users.userid')
        //     ->select('projek_header.kode_projek','projek_header.nama_projek','projek_header.tanggal','projek_header.userid','projek_header.lokasi_projek','users.nama')
        //     ->orderby('projek_header.userid','asc')
        //     ->get();
            // 'projek_header'=>$projek_header,
        return view('Client.index',[ 'users'=>$users,'client'=>$client,'new_date' => $new_date]);
    }
    public function permintaanuser(Request $request)
    {
        $kode_client =$request->id;
            $master_barang = DB::table('client')
            ->where('kode_client', $kode_client)
            ->get();
            return  $master_barang;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'userid' => 'required',
            'nama_projek' => 'required',
            'tanggal' => 'required',
            'lokasi_projek' => 'required',
            'kode_projek' => 'required',
            'permintaan.*.kode_client' => 'required',
            'permintaan.*.estimasi_projek' => 'required',
            'permintaan.*.mulai_projek' => 'required',
            'permintaan.*.selesai_projek' => 'required'
        ]);
        // dd($request->all());
     
        foreach ($request->permintaan as $items => $item) {
            if($item['kode_client']==""){
                return redirect('permintaan_projek')->with('gagal' , 'Nama Client Harus Dipilih');
            }else{
                // dd($request->all());
                $footer_projek = DB::table('footer_projek')->insert([
                    'kode_projek' => $request->kode_projek,
                    'kode_client' => $item['kode_client'],
                    'estimasi_projek' => $item['estimasi_projek'],
                    'mulai_projek' => $item['mulai_projek'],
                    'selesai_projek' => $item['selesai_projek']
                ]);
                if(is_null($footer_projek)) {            
                return redirect('permintaan_projek')->with('gagal' , 'Data Gagal Tersimpan');
                }
            }
        }
        $projek_header = DB::table('projek_header')->insert([
            'kode_projek' => $request->kode_projek,
            'nama_projek' => $request->nama_projek,
            'userid' => $request->lokasi_projek,
            'lokasi_projek' =>  $request->lokasi_projek,
            'tanggal' => $request->tanggal
        ]);
        if(!is_null($projek_header)) {            
            return redirect('permintaan_projek')->with('success' , 'Data Sukses Tersimpan');
        }    
        else {
            return redirect('permintaan_projek')->with('gagal' , 'Data Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
