<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,
        ['nama'=>'required|min:5'],
        ['departemen'=>'required'],
        ['userid'=>'required|min:5|unique:users'],
        ['email'=>'required|email|unique:users'],
        ['password'=>'min:5|required_with:confirmed|same:confirmed']  
        );

        $user = DB::table('users')->insert([
            'nama' => $request->nama,
            'userid' => $request->userid,
            'departemen' => $request->departemen,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'level'=> '2'
        ]);
        if(!is_null($user)) {            
            return redirect('/')->with('success' , 'Data Sukses Tersimpan');
        }    
        else {
            return redirect('/register')->with('gagal' , 'Data Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postlogin(Request $request)
    {
        $this->validate($request,
        
            ['userid'=>'required'],

            ['password'=>'required']  
        );
    //    dd($request->all());
        if(Auth::attempt($request->only('userid','password'))){
            $user_id = $request->userid;
            $nama = $request->nama;
            $password = $request->password;
            Session::put('userid' , $user_id);
            Session::put('password' , $password);
            Session::put('nama' , $nama);
            return redirect('/dashboard')->with('success','Selamat anda berhasil login');
        }
        return redirect('/')->with('credential','User id Atau Password Yang Anda Masukan Salah');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
