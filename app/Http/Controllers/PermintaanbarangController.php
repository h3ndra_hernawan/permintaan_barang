<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PDF;
use Notification;
use App\Notifications\MyFirstNotification;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class PermintaanbarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        date_default_timezone_set("Asia/Jakarta") ;
        $new_date = date('dHis')."/kd/PER_BRG/".date('m').'/'.date('Y'); 
        $users = DB::table('users')->get();
        $master_barang = DB::table('master_barang')->get();
        $footer_transaksi = DB::table('footer_transaksi')
            ->join('users', 'footer_transaksi.userid', '=', 'users.userid')
            ->select('footer_transaksi.kode_transaksi','footer_transaksi.jumlah','footer_transaksi.metodepembayaran','footer_transaksi.userid','footer_transaksi.tanggal_transaksi','users.nama')
            ->orderby('footer_transaksi.userid','asc')
            ->get();
        return view('PermintaanBarang.index',['footer_transaksi'=>$footer_transaksi, 'users'=>$users,'master_barang'=>$master_barang,'new_date' => $new_date]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datakaryawan(Request $request)
    {
        $group =$request->id;
            $nik= DB::table('data_karyawan')
            ->where('nik', $group)
            ->get();
            return  $nik;
    }
    public function permintaan_barang(Request $request)
    {
        $kode_barang =$request->id;
            $master_barang = DB::table('master_barang')
            ->where('kode_barang', $kode_barang)
            ->get();
            return  $master_barang;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request->all());
        $this->validate($request, [
            'metodepembayaran' => 'required',
            'nama' => 'required',
            'tanggal' => 'required',
            'created_at' => 'required',
            'permintaan.*.kode_barang' => 'required',
            'permintaan.*.lokasi' => 'required',
            'permintaan.*.tersedia' => 'required',
            'permintaan.*.kuantiti' => 'required',
            'permintaan.*.satuan' => 'required',
            'permintaan.*.totbayar' => 'required'
        ]);
        // dd($request->all());
        $total=0;
        foreach ($request->permintaan as $items => $item) {
            if($item['kode_barang']==""){
                return redirect('permintaan_barang')->with('gagal' , 'Nama Barang Harus Dipilih');
            }else{
                // dd($request->all());
                $header_transaksi = DB::table('header_transaksi')->insert([
                    'kode_transaksi' => $request->kode_transaksi,
                    'kode_barang' => $item['kode_barang'],
                    'kuantiti' => $item['kuantiti'],
                    'satuan' => $item['satuan'],
                    'totbayar' => $item['totbayar'],
                    'created_at' => $item['created_at']
                ]);
                $total +=  $item['totbayar'];
                if(is_null($header_transaksi)) {            
                return redirect('permintaan_barang')->with('gagal' , 'Data Gagal Tersimpan');
                }
            }
        }
        $footer_transaksi = DB::table('footer_transaksi')->insert([
            'kode_transaksi' => $request->kode_transaksi,
            'userid' => $request->nama,
            'metodepembayaran' => $request->metodepembayaran,
            'jumlah' =>  $total,
            'tanggal_transaksi' => $request->tanggal,
            'created_at' => $request->created_at
        ]);
        if(!is_null($footer_transaksi)) {            
            return redirect('permintaan_barang')->with('success' , 'Data Sukses Tersimpan');
        }    
        else {
            return redirect('permintaan_barang')->with('gagal' , 'Data Gagal Tersimpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $id = base64_decode($id);
      
        $header_transaksi = DB::table('header_transaksi')
            ->join('footer_transaksi', 'header_transaksi.kode_transaksi', '=', 'footer_transaksi.kode_transaksi')
            ->join('master_barang', 'header_transaksi.kode_barang', '=', 'master_barang.kode_barang')
            ->select('header_transaksi.*','master_barang.kode_barang','master_barang.nama_barang','footer_transaksi.userid','master_barang.nama_barang')
            ->where('header_transaksi.kode_transaksi', $id)
            ->orderby('header_transaksi.kode_transaksi','asc')
            ->get();
            // return $header_transaksi;
            return view('PermintaanBarang.detail',['header_transaksi'=> $header_transaksi]);
    }

    public function laporan()
    {
        return view('laporan.index');
    }
    public function searchminimarket(Request $request)
    {
       
     
        $header_transaksi = DB::table('header_transaksi')
        ->join('footer_transaksi', 'header_transaksi.kode_transaksi', '=', 'footer_transaksi.kode_transaksi')
        ->join('master_barang', 'header_transaksi.kode_barang', '=', 'master_barang.kode_barang')
        ->select('header_transaksi.*','master_barang.kode_barang','footer_transaksi.tanggal_transaksi','footer_transaksi.metodepembayaran','master_barang.nama_barang','footer_transaksi.userid','master_barang.nama_barang')
        ->whereBetween('footer_transaksi.tanggal_transaksi', [$request->tanggal_awal, $request->tanggal_akhir])
        ->orderby('header_transaksi.kode_transaksi','asc')
        ->get();
        return $header_transaksi;
      
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lapminimarketexcell(Request $request)
    {
        
        $header_transaksi = DB::table('header_transaksi')
        ->join('footer_transaksi', 'header_transaksi.kode_transaksi', '=', 'footer_transaksi.kode_transaksi')
        ->join('master_barang', 'header_transaksi.kode_barang', '=', 'master_barang.kode_barang')
        ->select('header_transaksi.*','master_barang.kode_barang','footer_transaksi.tanggal_transaksi','footer_transaksi.metodepembayaran','master_barang.nama_barang','footer_transaksi.userid','master_barang.nama_barang')
        ->whereBetween('footer_transaksi.tanggal_transaksi', [$request->tanggal_awal, $request->tanggal_akhir])
        ->orderby('header_transaksi.kode_transaksi','asc')
        ->get();
        
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->setTitle('Laporan Pendapatan Minimarket');

        $sheet = $spreadsheet->getActiveSheet()->getRowDimension('4')->setRowHeight(30);
        $sheet = $spreadsheet->getActiveSheet()->freezePane('A5');
        $sheet = $spreadsheet->getActiveSheet()->getStyle('A8:I8')->getFont()->setBold(true);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('A8:I8')->getAlignment()->setHorizontal('center')->setVertical('center');
        $sheet = $spreadsheet->getActiveSheet()->getStyle('A8:I8')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('A8:I8')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('A8:I8')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('A8:I8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $sheet = $spreadsheet->getActiveSheet()->getStyle('C1:F1')->getFont()->setBold(true);
        $sheet = $spreadsheet->getActiveSheet()->getCell('C1')->setValue("LAPORAN PER TANGGAL AWAL");
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);

        $sheet = $spreadsheet->getActiveSheet()->getStyle('D1:D1')->getFont()->setBold(true);
        $sheet = $spreadsheet->getActiveSheet()->getCell('D1')->setValue($request->tanggal_awal);
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
   
        $sheet = $spreadsheet->getActiveSheet()->getStyle('C2:C2')->getFont()->setBold(true);
        $sheet = $spreadsheet->getActiveSheet()->getCell('C2')->setValue("LAPORAN PERTANGGAL AKHIR");
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);

        $sheet = $spreadsheet->getActiveSheet()->getStyle('D2:D2')->getFont()->setBold(true);
        $sheet = $spreadsheet->getActiveSheet()->getCell('D2')->setValue($request->tanggal_akhir);
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            // 
        $sheet = $spreadsheet->getActiveSheet()->getCell('A8')->setValue("NO");
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('A8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $sheet = $spreadsheet->getActiveSheet()->getCell('B8')->setValue("Tanggal Transaksi");
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('B8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        
        $sheet = $spreadsheet->getActiveSheet()->getCell('C8')->setValue("Kode Transaksi");     
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('C8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        
        $sheet = $spreadsheet->getActiveSheet()->getCell('D8')->setValue("Nama Barang");     
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('D8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        
        $sheet = $spreadsheet->getActiveSheet()->getCell('E8')->setValue("Qty beli");     
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(40);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('E8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $sheet = $spreadsheet->getActiveSheet()->getCell('F8')->setValue("Harga Satuan");     
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(40);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('F8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $sheet = $spreadsheet->getActiveSheet()->getCell('G8')->setValue("Metode Pembayaran");     
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('G8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        
        $sheet = $spreadsheet->getActiveSheet()->getCell('H8')->setValue("Total Bayar");     
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('H8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        
        $sheet = $spreadsheet->getActiveSheet()->getCell('I8')->setValue("User Input");     
        $sheet = $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        $sheet = $spreadsheet->getActiveSheet()->getStyle('I8')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


        $counter_column = 8;
        if(!empty($header_transaksi)){
            $no=0;
            $jmsatuan = 0;
            $jmtotbayar = 0;
            $jumqty=0;
            foreach ($header_transaksi as $result) {
                $counter_column += 1;
                $sheet = $spreadsheet->getActiveSheet()->getCell('A'.$counter_column)->setValue($no+=1);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('A'.$counter_column)->getAlignment()->setHorizontal('left')->setVertical('left');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('A'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('A'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('A'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('A'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet = $spreadsheet->getActiveSheet()->getCell('B'.$counter_column)->setValue($result->tanggal_transaksi);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('B'.$counter_column)->getAlignment()->setHorizontal('left')->setVertical('left');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('B'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('B'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('B'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('B'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet = $spreadsheet->getActiveSheet()->getCell('C'.$counter_column)->setValue($result->kode_transaksi);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('C'.$counter_column)->getAlignment()->setHorizontal('left')->setVertical('left');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('C'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('C'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('C'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('C'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet = $spreadsheet->getActiveSheet()->getCell('D'.$counter_column)->setValue($result->nama_barang );
                $sheet = $spreadsheet->getActiveSheet()->getStyle('D'.$counter_column)->getAlignment()->setHorizontal('left')->setVertical('left');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('D'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('D'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('D'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('D'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                
                $sheet = $spreadsheet->getActiveSheet()->getCell('E'.$counter_column)->setValue($result->kuantiti);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getAlignment()->setHorizontal('left')->setVertical('left');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet = $spreadsheet->getActiveSheet()->getCell('F'.$counter_column)->setValue(number_format($result->satuan) );
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getAlignment()->setHorizontal('right')->setVertical('right');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
             
                $sheet = $spreadsheet->getActiveSheet()->getCell('G'.$counter_column)->setValue($result->metodepembayaran);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getAlignment()->setHorizontal('left')->setVertical('left');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
             
                $sheet = $spreadsheet->getActiveSheet()->getCell('H'.$counter_column)->setValue(number_format($result->totbayar) );
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getAlignment()->setHorizontal('right')->setVertical('right');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
             
                 $sheet = $spreadsheet->getActiveSheet()->getCell('I'.$counter_column)->setValue($result->userid );
                $sheet = $spreadsheet->getActiveSheet()->getStyle('I'.$counter_column)->getAlignment()->setHorizontal('left')->setVertical('left');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('I'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('I'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('I'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('I'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $jmsatuan += $result->satuan;
                $jmtotbayar += $result->totbayar;
                $jumqty += $result->kuantiti;
            }
                $sheet = $spreadsheet->getActiveSheet()->getCell('E'.$counter_column += 1)->setValue($jumqty);     
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getAlignment()->setHorizontal('right')->setVertical('right');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('E'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet = $spreadsheet->getActiveSheet()->getCell('F'.$counter_column)->setValue(number_format($jmsatuan));     
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getAlignment()->setHorizontal('right')->setVertical('right');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('F'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
           
                $sheet = $spreadsheet->getActiveSheet()->getCell('G'.$counter_column)->setValue('');     
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getAlignment()->setHorizontal('right')->setVertical('right');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('G'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                $sheet = $spreadsheet->getActiveSheet()->getCell('H'.$counter_column)->setValue(number_format($jmtotbayar));    
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getAlignment()->setHorizontal('right')->setVertical('right');
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet = $spreadsheet->getActiveSheet()->getStyle('H'.$counter_column)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
          
        }


        $writer = new Xlsx($spreadsheet);
        $filename = 'LAPORAN_MINIMARKET';
        $writer->save(storage_path($filename.'.xlsx'));
        Session::flash('download.in.the.next.request', '/data/get-document/'.$filename.'.xlsx');
        return redirect('/laporan')->with('success' , 'Berhasil download excell , tunggu beberapa menit!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getDocument($document)
    {
    //    return $document;
        return response()->download(storage_path($document));
    }
}
