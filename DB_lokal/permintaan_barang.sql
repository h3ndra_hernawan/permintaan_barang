-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2021 at 05:19 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `permintaan_barang`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_karyawan`
--

CREATE TABLE `data_karyawan` (
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departemen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_karyawan`
--

INSERT INTO `data_karyawan` (`nik`, `nama`, `departemen`, `created_at`, `updated_at`) VALUES
('123', 'tes', 'it', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('1234', 'tes demo', 'hrd', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `footer_permintaan`
--

CREATE TABLE `footer_permintaan` (
  `kode_permintaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_permintaan` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footer_permintaan`
--

INSERT INTO `footer_permintaan` (`kode_permintaan`, `nik`, `tanggal_permintaan`, `created_at`, `updated_at`) VALUES
('10204325/kd/PER_BRG/09/2021', '123', '2021-09-10 13:44:55', '2021-09-10 13:43:25', NULL),
('10213317/kd/PER_BRG/09/2021', '1234', '2021-09-10 14:33:49', '2021-09-10 14:33:17', NULL),
('10213349/kd/PER_BRG/09/2021', '123', '2021-09-10 14:36:36', '2021-09-10 14:33:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `header_permintaan`
--

CREATE TABLE `header_permintaan` (
  `header_id` bigint(20) UNSIGNED NOT NULL,
  `kode_permintaan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_barang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kuantiti` int(11) NOT NULL,
  `satuan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `header_permintaan`
--

INSERT INTO `header_permintaan` (`header_id`, `kode_permintaan`, `kode_barang`, `kuantiti`, `satuan`, `keterangan`, `created_at`, `updated_at`) VALUES
(5, '10204325/kd/PER_BRG/09/2021', 'ab001', 12, 'km', 'dddd', '2021-09-10 13:43:25', NULL),
(6, '10204325/kd/PER_BRG/09/2021', 'ab002', 233, 'kg', 'dddd', '2021-09-10 13:43:25', NULL),
(7, '10213317/kd/PER_BRG/09/2021', 'ab001', 23, 'km', 'eee', '2021-09-10 14:33:17', NULL),
(8, '10213349/kd/PER_BRG/09/2021', 'ab001', 22, 'km', 'tyrtr', '2021-09-10 14:33:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_barang`
--

CREATE TABLE `master_barang` (
  `kode_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_barang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tersedia` int(11) NOT NULL,
  `satuan` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_barang`
--

INSERT INTO `master_barang` (`kode_barang`, `nama_barang`, `lokasi`, `tersedia`, `satuan`, `created_at`, `updated_at`) VALUES
('ab001', 'tes barang', '001', 31, 'km', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ab002', 'tes barang 2', '002', 30, 'kg', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_karyawan`
--
ALTER TABLE `data_karyawan`
  ADD UNIQUE KEY `data_karyawan_nik_unique` (`nik`);

--
-- Indexes for table `footer_permintaan`
--
ALTER TABLE `footer_permintaan`
  ADD UNIQUE KEY `footer_permintaan_kode_permintaan_unique` (`kode_permintaan`);

--
-- Indexes for table `header_permintaan`
--
ALTER TABLE `header_permintaan`
  ADD PRIMARY KEY (`header_id`);

--
-- Indexes for table `master_barang`
--
ALTER TABLE `master_barang`
  ADD UNIQUE KEY `master_barang_kode_barang_unique` (`kode_barang`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `header_permintaan`
--
ALTER TABLE `header_permintaan`
  MODIFY `header_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
