<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'LoginController@index')->name('/');
Route::get('register','LoginController@register');
Route::post('/user/create','LoginController@store');
Route::post('/postlogin','LoginController@postlogin');
Route::get('/dashboard','DashboardController@index');
Route::post('/dashboard','DashboardController@index');
Route::post('logout' , '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');


// client
Route::get('/projek_user','ProjekUserController@index');
Route::get('/data/permintaan_client' , 'ProjekUserController@permintaanuser');
Route::resource('permintaan_projek', 'ProjekUserController');

// transaksi
Route::get('/transaksi','PermintaanbarangController@index');
Route::get('/laporan','PermintaanbarangController@laporan');
Route::get('/data/search-laporan-mini-marker', 'PermintaanbarangController@searchminimarket');
Route::post('/data/lap_buku_besar_excell', 'PermintaanbarangController@lapminimarketexcell');

Route::get('/data/get-document/{document}' , 'PermintaanbarangController@getDocument');

Route::get('/data/permintaan_barang' , 'PermintaanbarangController@permintaan_barang');
Route::resource('permintaan_barang', 'PermintaanbarangController');
Route::get('/data/permintaan_barang/{id}' , 'PermintaanbarangController@detail');