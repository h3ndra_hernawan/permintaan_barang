@extends('layouts.master')
@section('styles')
<!-- DataTables -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <link rel="stylesheet" href="{{url('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
  .alert-message {
    color: red;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
@endsection
@section('content')

<div class="content-wrapper">
    <div class="content">
         <div class="container-fluid">
            <section class="content">
                    <div class="card card-primary card-outline">
                        <!-- <div class="card-body">
                            <p>Selamat Datang</p>
                            <strong><i class="fas fa-users"></i>  Halaman User</strong>
                        </div> -->
                        <!-- /.card-body -->
                        <!-- <hr> -->
                        <div class="card-header">
                            <a href="/dashboard" class="breadcrumb-item"><i class="fa fa-home"></i> Home</a>
                            <span class="breadcrumb-item active">Data Pelanggan</span>
                        </div> <!-- /.card-body -->
                    </div>
            </section>
        </div>
    </div>    
    <div class="content">
        <section class="content">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Daftar Data Pelanggan</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if (session('success'))
                                    <div class="alert alert-success border-0 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                        {{ session('success') }}.
                                    </div>
                                    @endif

                                    @if (session('message'))
                                    <div class="alert alert-danger border-0 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                        {{ session('message') }}.
                                    </div>
                                    @endif
                                    <a class="btn btn-success" href="javascript:void(0)" id="createNewUser"> <i class="fas fa-user-plus"></i> Tambah Data</a>          
                                    <br>
                                    <hr>
                                    <div class="panel-body">       
                                        <div style="overflow-x:auto;">
                                            <table class="table table-bordered table-hover datatable-basic">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>No Ktp</th>
                                                        <th>Nama</th>
                                                        <th>Tanggal Lahir</th>
                                                        <th>Alamat</th>
                                                        <th>No Tlp</th>
                                                        <th>Jenis Kelamin</th>
                                                        <th>input</th>
                                                        <th width="180px">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>    
                                            </table>
                                        </div>   
                                                <div class="modal fade" id="ajaxModel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="modelHeading"></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="alert alert-danger print-error-msg" style="display:none">
                                                                    <ul></ul>
                                                                </div>
                                                                <span aria-hidden="true">&times;</span>
                                                                <form id="ItemForm" name="ItemForm" class="form-horizontal">
                                                                    <input type="hidden" name="id" id="id">
                                                                    <input type="hidden" name="username" id="username" value="{{auth()->user()->nama}}">
                                                                    <div class="form-group{{$errors->has('no_ktp') ? ' has-error' : ''}}">
                                                                            <label for="nama" class="col-lg-4 control-label">No KTP</label>
                                                                            <div class="col-lg-9">
                                                                                <input type="text" class="form-control" id="no_ktp" name="no_ktp" placeholder="Masukan No KTP" maxlength="50" required="" value="{{old('no_ktp')}}">
                                                                                @if($errors->has('no_ktp'))
                                                                                <span class="help-block">{{$errors->first('no_ktp')}}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    <div class="form-group{{$errors->has('nama') ? ' has-error' : ''}}">
                                                                            <label for="nama" class="col-lg-5 control-label">Nama lengkap</label>
                                                                            <div class="col-lg-9">
                                                                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Lengkap" maxlength="50" required="" value="{{old('nama')}}">
                                                                                @if($errors->has('nama'))
                                                                                <span class="help-block">{{$errors->first('nama')}}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    <div class="form-group{{$errors->has('tgl_lahir') ? ' has-error' : ''}}">
                                                                            <label for="tgl_lahir" class="col-lg-3 control-label">Tgl Lahir</label>
                                                                            <div class="col-lg-9">
                                                                            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Masukan Tanggal lhir" maxlength="50" required="" value="{{old('tgl_lahir')}}">
                                                                                @if($errors->has('tgl_lahir'))
                                                                                <span class="help-block">{{$errors->first('tgl_lahir')}}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    <div class="form-group{{$errors->has('alamat') ? ' has-error' : ''}}">
                                                                        <label for="alamat" class="col-lg-5 control-label">Alamat Lengkap</label>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                            <textarea id="alamat" name="alamat" rows="3" cols="3" class="form-control"></textarea>
                                                                                <span id="alamatError" class="alert-message"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                
                                                                    <div class="form-group{{$errors->has('tlp') ? ' has-error' : ''}}">
                                                                            <label for="tlp" class="col-lg-5 control-label">No tlp</label>
                                                                            <div class="col-lg-9">
                                                                                <div class="input-group mb-3">
                                                                                    <input type="number" id="tlp" name="tlp" class="form-control" placeholder="Masukan tlp">
                                                                                    <span id="tlpError" class="alert-message"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group{{$errors->has('jenis_kelamin') ? ' has-error' : ''}}">
                                                                            <label for="jenis_kelamin" class="col-lg-3 control-label">Jenis Kelamin</label>
                                                                            <div class="col-lg-9">
                                                                            <select class="form-control"  id="jenis_kelamin" name="jenis_kelamin" id="exampleFormControlSelect1">
                                                                                <option value="0" disabled selected>Pilih Kategori Produk</option>
                                                                                <option value="Laki-Laki">Laki-Laki</option>
                                                                            <option value="Perempuan">Perempuan</option>
                                                                            </select>
                                                                            @if($errors->has('jenis_kelamin'))
                                                                                <span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
                                                                            @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                        <button type="button" class="btn btn-link" data-dismiss="modal">&times;Close</button>
                                                                        <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fas fa-database"></i> Save</button>
                                                                        </div>  
                                                                </form>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>    
                                        </div>    
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </section> 
                    </div>   
            </div>
    </div>              
</body>

  @endsection
  @section('javascripts')
<!-- DataTables -->
<script src="{{url('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{url('admin/plugins/jquery-ui/interactions.min.js') }}"></script>
<script src="{{url('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{url('admin/plugins/forms/selects/select2.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="{{url('admin/demo_pages/datatables_basic.js') }}"></script> -->
<script src="{{url('admin/demo_pages/form_select2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.datatable-basic').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('pelanggan.index') }}",
        columns: [
            { "data": null,"sortable": false, 
                render: function (data, type, row, meta) {
                 return meta.row + meta.settings._iDisplayStart + 1;
                }  
            },
            {data: 'no_ktp', name: 'no_ktp'},
            {data: 'nama', name: 'nama'},
            {data: 'tgl_lahir', name: 'tgl_lahir'},
            {data: 'alamat', name: 'alamat'},
            {data: 'tlp', name: 'tlp'},
            {data: 'jenis_kelamin', name: 'jenis_kelamin'},
            {data: 'username', name: 'username'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewUser').click(function () {
        $('#saveBtn').val("create-user");
        $('#id').val('');
        $('#ItemForm').trigger("reset");
        $('#modelHeading').html("Tambah Data");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('pelanggan.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Edit User");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#no_ktp').val(data.no_ktp);
          $('#nama').val(data.nama);
          $('#tgl_lahir').val(data.tgl_lahir);
          $('#alamat').val(data.alamat);
          $('#tlp').val(data.tlp);
          $('#username').val(data.username);
          $('#jenis_kelamin').val(data.jenis_kelamin);
      })
   });
   $('body').on('click', '.showItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('pelanggan.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Show User");
          $('#saveBtn').val("show-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#no_ktp').val(data.no_ktp);
          $('#nama').val(data.nama);
          $('#tgl_lahir').val(data.tgl_lahir);
          $('#alamat').val(data.alamat);
          $('#tlp').val(data.tlp);
          $('#username').val(data.username);
          $('#jenis_kelamin').val(data.jenis_kelamin);         
      })
      $('#saveBtn').modal('hide');
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
          data: $('#ItemForm').serialize(),
          url: "{{ route('pelanggan.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
            if($.isEmptyObject(data.error)){
              $('#ItemForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              swal("Data pelanggan Berhasil Di Simpan", data.message, "success");
              table.draw();
            }else{
              swal("Harap Cek inputan Data", data.message, "error");
                printErrorMsg(data.error);
            }
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    $('body').on('click', '.deleteItem', function () {

      var user_id = $(this).data("id");
      swal({
            title: "Delete?",
            text: "Yakin Mau Delete ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yakin",
            cancelButtonText: "Tidak!",
            reverseButtons: !0
        }).then(function (e) {
          if (e.value === true) {
            $.ajax({
                type: "DELETE",
                url: "{{ route('pelanggan.store') }}"+'/'+user_id,
                success: function (data) {
                  $('#ItemForm').trigger("reset");
                  $('#ajaxModel').modal('hide');
                  swal("Data pelanggan Berhasil Di Delete!", data.message, "success");
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
              });
          } else {
                e.dismiss;
            }  
        }, function (dismiss) {
          return false;
      })
    });
     
  });
</script>
@endsection