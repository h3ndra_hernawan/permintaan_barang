@extends('layouts.master')
@section('styles')
<!-- DataTables -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{url('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
  .alert-message {
    color: red;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
@endsection
@section('content')

<div class="content-wrapper">
    <div class="content">
         <div class="container-fluid">
            <section class="content">
                    <div class="card card-primary card-outline">
                        <!-- <div class="card-body">
                            <p>Selamat Datang</p>
                            <strong><i class="fas fa-users"></i>  Halaman User</strong>
                        </div> -->
                        <!-- /.card-body -->
                        <!-- <hr> -->
                        <div class="card-header">
                            <a href="/dashboard" class="breadcrumb-item"><i class="fa fa-home"></i> Home</a>
                            <span class="breadcrumb-item active">Data Pesanan</span>
                        </div> <!-- /.card-body -->
                    </div>
            </section>
        </div>
    </div>    
    <div class="content">
        <section class="content">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Daftar Data Pesanan</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if (session('success'))
                                    <div class="alert alert-success border-0 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                        {{ session('success') }}.
                                    </div>
                                    @endif

                                    @if (session('message'))
                                    <div class="alert alert-danger border-0 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                        {{ session('message') }}.
                                    </div>
                                    @endif
                                    
                                    <a class="btn btn-success" href="javascript:void(0)" id="createNewUser"> <i class="fas fa-user-plus"></i> Tambah Data</a>          
                                    <br>
                                    <hr>
                                    <div class="panel-body">       
                                        <div style="overflow-x:auto;">
                                            <table class="table table-bordered table-hover datatable-basic">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Id Pesanan</th>
                                                        <th>Nama Pelanggan</th>
                                                        <th>Nama Produk</th>
                                                        <th>Tanggl Pembelian</th>
                                                        <th>Jumlah Beli</th>
                                                        <th>Total Beli</th>
                                                        <th>Harga</th>
                                                        <th>input</th>
                                                        <th width="180px">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>    
                                            </table>
                                        </div>   
                                            <div class="modal fade" id="ajaxModel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="modelHeading"></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="alert alert-danger print-error-msg" style="display:none">
                                                            <ul></ul>
                                                            </div>
                                                            <span aria-hidden="true">&times;</span>
                                                            <form id="ItemForm" name="ItemForm" class="form-horizontal">
                                                                <input type="hidden" name="id" id="id">
                                                                <input type="hidden" name="username" id="username" value="{{auth()->user()->nama}}">
                                                                <div class="form-group{{$errors->has('id_pesanan') ? ' has-error' : ''}}">
                                                                        <label for="nama" class="col-lg-4 control-label">Id Pesanan</label>
                                                                    <div class="col-lg-9">
                                                                            <input type="text" class="form-control" id="id_pesanan" name="id_pesanan" placeholder="Masukan Id Pesanan" maxlength="50" required="" value="{{old('id_pesanan')}}">
                                                                            @if($errors->has('id_pesanan'))
                                                                            <span class="help-block">{{$errors->first('id_pesanan')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('no_ktp') ? ' has-error' : ''}}">
                                                                        <label for="nama" class="col-lg-4 control-label">No Ktp</label>
                                                                        <div class="col-lg-9">
                                                                        <select class="form-control select-search @error('no_ktp') is-invalid @enderror" name="no_ktp" data-fouc id="no_ktp">
                                                                            <option value="0" disable="true" selected="true">Pilih No Ktp</option>
                                                                            @foreach ($pelanggan as $key => $value)
                                                                                <option value="{{$value->nama}}">{{ $value->no_ktp }}</option>
                                                                                @endforeach
                                                                        </select>
                                                                            @if($errors->has('no_ktp'))
                                                                            <span class="help-block">{{$errors->first('no_ktp')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('nama') ? ' has-error' : ''}}">
                                                                        <label for="nama" class="col-lg-5 control-label">Nama Pelanggan</label>
                                                                        <div class="col-lg-9">
                                                                        <select class="form-control select-search @error('nama') is-invalid @enderror" name="nama" data-fouc id="nama">
                                                                            <option value="0" disable="true" selected="true">Pilih Nama Pelanggan</option>
                                                                        </select>
                                                                            @if($errors->has('nama'))
                                                                            <span class="help-block">{{$errors->first('nama')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group{{$errors->has('kode_produk') ? ' has-error' : ''}}">
                                                                        <label for="nama" class="col-lg-4 control-label">Kode Produk</label>
                                                                        <div class="col-lg-9">
                                                                        <select class="form-control select-search @error('kode_produk') is-invalid @enderror" name="kode_produk" data-fouc id="kode_produk">
                                                                            <option value="0" disable="true" selected="true">Pilih Kode Produk</option>
                                                                            @foreach ($produk as $key => $value)
                                                                                <option value="{{$value->nama_produk}}">{{ $value->kode_produk }}</option>
                                                                                @endforeach
                                                                        </select>
                                                                            @if($errors->has('kode_produk'))
                                                                            <span class="help-block">{{$errors->first('kode_produk')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('nama_produk') ? ' has-error' : ''}}">
                                                                        <label for="nama_produk" class="col-lg-3 control-label">Nama Produk</label>
                                                                        <div class="col-lg-9">
                                                                        <select class="form-control select-search @error('nama_produk') is-invalid @enderror" name="nama_produk" data-fouc id="nama_produk">
                                                                            <option value="0" disable="true" selected="true">Pilih Nama Produk</option>
                                                                        </select>
                                                                            @if($errors->has('nama_produk'))
                                                                            <span class="help-block">{{$errors->first('nama_produk')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('merek_produk') ? ' has-error' : ''}}">
                                                                        <label for="merek_produk" class="col-lg-3 control-label">Merek Produk</label>
                                                                        <div class="col-lg-9">
                                                                        <select class="form-control select-search @error('merek_produk') is-invalid @enderror" name="merek_produk" data-fouc id="merek_produk">
                                                                            <option value="0" disable="true" selected="true">Pilih Merek Produk</option>
                                                                        </select>
                                                                            @if($errors->has('merek_produk'))
                                                                            <span class="help-block">{{$errors->first('merek_produk')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('tgl_pembelian') ? ' has-error' : ''}}">
                                                                        <label for="tgl_pembelian" class="col-lg-3 control-label">Tgl Pembelian</label>
                                                                        <span class="error"><?php echo $errors->first('tgl_pembelian') ?></span>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                                <input type="date" id="tgl_pembelian" class="form-control " name="tgl_pembelian" placeholder="Masukan Tgl Pembelian">
                                                                            
                                                                                @if($errors->has('tgl_pembelian'))
                                                                            <span class="help-block">{{$errors->first('tgl_pembelian')}}</span>
                                                                        @endif
                                                                            </div>
                                                                        </div>    
                                                                    </div>
            
                                                                    <div class="form-group{{$errors->has('jumlah_beli') ? ' has-error' : ''}}">
                                                                        <label for="jumlah_beli" class="col-lg-5 control-label">Jumlah Beli</label>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                                <input type="number" id="jumlah_beli" name="jumlah_beli" class="form-control" placeholder="Masukan jumlah_beli" onFocus="startCalc()">
                                                                                <span id="jumlah_beliError" class="alert-message"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('harga') ? ' has-error' : ''}}">
                                                                        <label for="harga" class="col-lg-5 control-label">Harga</label>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                                <input type="text" id="harga" name="harga" class="form-control" placeholder="Masukan harga" onFocus="startCalc()">
                                                                                <span id="hargaError" class="alert-message"></span>
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('total_bayar') ? ' has-error' : ''}}">
                                                                        <label for="total_bayar" class="col-lg-5 control-label">Total Beli</label>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                                <input type="number" id="total_bayar" name="total_bayar" class="form-control" placeholder="Masukan Total Beli" onchange="tryNumberFormat(this.form.thirdBox);" readonly>
                                                                                <span id="total_bayarError" class="alert-message"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                    <button type="button" class="btn btn-link" data-dismiss="modal">&times;Close</button>
                                                                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fas fa-database"></i> Save</button>
                                                                    </div>
                                                            </form>
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>    
                                        </div>    
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </section> 
            </div>   
        </div>
    </div>              
</body>

  @endsection
  @section('javascripts')
<!-- DataTables -->
<script src="{{url('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{url('admin/plugins/jquery-ui/interactions.min.js') }}"></script>
<script src="{{url('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{url('admin/plugins/forms/selects/select2.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="{{url('admin/demo_pages/datatables_basic.js') }}"></script> -->
<script src="{{url('admin/demo_pages/form_select2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
<!-- jumlah otomatis -->
<script language="javascript">
 
function startCalc(){
interval = setInterval("calc()",1);}
function calc(){
trie = document.ItemForm.jumlah_beli.value;
four = document.ItemForm.harga.value;
document.ItemForm.total_bayar.value = (four * 1) * (trie * 1);
}

function stopCalc(){
clearInterval(interval);}

  </script>
<!-- list pelanggan -->
<script>
      $('#no_ktp').on('change', function(e){
        console.log('cek data',e);
        var nama = e.target.value;
        $.get('/listpelanggan?nama=' + nama,function(data) {
          console.log('cek',data);
          $('#nama').empty();
          $('#nama').append('<option value="0" disable="true" selected="true">=== Select Nama Pelanggan===</option>');
          $.each(data, function(index, regenciesObj){
            console.log('ok',data);
            $('#nama').append('<option value="'+ regenciesObj.nama +'">'+ regenciesObj.nama +'</option>');
          })
        });
      });
</script>
<!-- list produk -->
<script>
      $('#kode_produk').on('change', function(e){
        console.log('cek data',e);
        var nama_produk = e.target.value;
        $.get('/listmerekproduk?nama_produk=' + nama_produk,function(data) {
          console.log('cek',data);
          $('#nama_produk').empty();
          $('#nama_produk').append('<option value="0" disable="true" selected="true">=== Select Nama Produk ===</option>');

          $('#merek_produk').empty();
          $('#merek_produk').append('<option value="0" disable="true" selected="true">=== Select Merek Produk===</option>');
          $.each(data, function(index, regenciesObj){
            console.log('ok',data);
            $('#nama_produk').append('<option value="'+ regenciesObj.nama_produk +'">'+ regenciesObj.nama_produk +'</option>');
          })
        });
      });

      $('#nama_produk').on('change', function(e){
        console.log(e);
        var nama_produk = e.target.value;
        $.get('/listmerekproduk?nama_produk=' + nama_produk,function(data) {
          console.log(data);
          $('#merek_produk').empty();
          $('#merek_produk').append('<option value="0" disable="true" selected="true">=== Select Merek Produk===</option>');

          $.each(data, function(index, districtsObj){
            $('#merek_produk').append('<option value="'+ districtsObj.merek_produk +'">'+ districtsObj.merek_produk +'</option>');
          })
        });
      });
      $('#merek_produk').on('change', function(e){
        console.log(e);
        var nama_produk = e.target.value;
        $.get('/listhargaproduk?nama_produk=' + nama_produk,function(data) {
          console.log(data);
          $('#harga').empty();
          $('#harga').append('<option value="0" disable="true" selected="true">=== Select Merek Produk===</option>');

          $.each(data, function(index, districtsObj){
            $('#harga').append('<option value="'+ districtsObj.merek_produk +'">'+ districtsObj.harga +'</option>');
          })
        });
      });

</script>
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.datatable-basic').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('pesanan.index') }}",
        columns: [
            { "data": null,"sortable": false, 
                render: function (data, type, row, meta) {
                 return meta.row + meta.settings._iDisplayStart + 1;
                }  
            },
            {data: 'id_pesanan', name: 'id_pesanan'},
            {data: 'id_pelanggan', name: 'id_pelanggan'},
            {data: 'kode_produk', name: 'kode_produk'},
            {data: 'tgl_pembelian', name: 'tgl_pembelian'},
            {data: 'jumlah_beli', name: 'jumlah_beli'},
            {data: 'total_bayar', name: 'total_bayar'},
            {data: 'harga', name: 'harga'},
            {data: 'username', name: 'username'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewUser').click(function () {
        $('#saveBtn').val("create-user");
        $('#id').val('');
        $('#ItemForm').trigger("reset");
        $('#modelHeading').html("Tambah Data");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('pesanan.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Edit User");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#id_pesanan').val(data.id_pesanan);
          $('#id_pelanggan').val(data.id_pelanggan);
          $('#kode_produk').val(data.kode_produk);
          $('#tgl_pembelian').val(data.tgl_pembelian);
          $('#jumlah_beli').val(data.jumlah_beli);
          $('#total_bayar').val(data.total_bayar);
          $('#username').val(data.username);
      })
   });
   $('body').on('click', '.showItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('pesanan.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Show User");
          $('#saveBtn').val("show-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#id_pesanan').val(data.id_pesanan);
          $('#id_pelanggan').val(data.id_pelanggan);
          $('#kode_produk').val(data.kode_produk);
          $('#tgl_pembelian').val(data.tgl_pembelian);
          $('#jumlah_beli').val(data.jumlah_beli);
          $('#total_bayar').val(data.total_bayar);
          $('#username').val(data.username);
         
      })
      $('#saveBtn').modal('hide');
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
          data: $('#ItemForm').serialize(),
          url: "{{ route('pesanan.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
            if($.isEmptyObject(data.error)){
              $('#ItemForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              swal("Data pesanan Berhasil Di Simpan", data.message, "success");
              table.draw();
            }else{
              swal("Harap Cek inputan Data", data.message, "error");
                printErrorMsg(data.error);
            }
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    $('body').on('click', '.deleteItem', function () {

      var user_id = $(this).data("id");
      swal({
            title: "Delete?",
            text: "Yakin Mau Delete ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yakin",
            cancelButtonText: "Tidak!",
            reverseButtons: !0
        }).then(function (e) {
          if (e.value === true) {
            $.ajax({
                type: "DELETE",
                url: "{{ route('pesanan.store') }}"+'/'+user_id,
                success: function (data) {
                  $('#ItemForm').trigger("reset");
                  $('#ajaxModel').modal('hide');
                  swal("Data pesanan Berhasil Di Delete!", data.message, "success");
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
              });
          } else {
                e.dismiss;
            }  
        }, function (dismiss) {
          return false;
      })
    });
     
  });
</script>
@endsection