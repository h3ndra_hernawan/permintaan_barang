<!DOCTYPE html>
<html>
@section('title', 'Register')

@include('auth.header')
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
   
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <div class="text-center mb-3">
        <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
         <p class="login-box-msg">Register a new membership</p>
      </div>
      <form action="/user/create" method="POST" enctype="multipart/form-data">
      {{csrf_field()}}
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          <input type="text" name="nama" class="form-control" placeholder="Full name">
        </div>
        <span class="alert-message"><?php echo $errors->first('nama') ?></span>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <input type="text" name="userid" class="form-control" placeholder="Username">
        </div>
        <span class="alert-message"><?php echo $errors->first('userid') ?></span>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <input type="text" name="departemen" class="form-control" placeholder="Departemen">
        </div>
        <span class="alert-message"><?php echo $errors->first('departemen') ?></span>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <input type="email" name="email" class="form-control" placeholder="Email">
        </div>
        <span class="alert-message"><?php echo $errors->first('email') ?></span>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
            <span id="show-password" class="fa fa-eye"></span>
            </div>
          </div>
        </div>
        <span class="alert-message"><?php echo $errors->first('password') ?></span>
        <div class="input-group mb-3">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <input type="password" id="confirmed" name="confirmed" class="form-control" placeholder="Retype password">
          <div class="input-group-append">
            <div class="input-group-text">
            <span id="confirmed-password" class="fa fa-eye"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <p class="mb-0">
            Do you have account?
            <a href="{{ route('/') }}" class="text-center">Log in</a>
            </p>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-success btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<script>
    $('#show-password').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('#password').attr('type', 'text');
        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');
      } else {
        $('#password').attr('type', 'password');
        $(this).removeClass('fa-eye-slash');
        $(this).addClass('fa-eye');
      }
    })
    $('#confirmed-password').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('#confirmed').attr('type', 'text');
        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');
      } else {
        $('#confirmed').attr('type', 'password');
        $(this).removeClass('fa-eye-slash');
        $(this).addClass('fa-eye');
      }
    })
  </script>
</body>
</html>
