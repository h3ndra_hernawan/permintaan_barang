<!DOCTYPE html>
<html>
<style>
  @media screen and (max-height: 600px) {
    .login-box {
      margin-top: 0;
      top: 10px;
    }
  }
  @media screen and (max-width: 400px) {
    .login-box {
      left: 5px;
      margin-left: 5px;
      min-width: 283px;
      right: 10px;
      margin-bottom: 10px;
      width: auto;
    }
    .login-form .form-control {
      width: 140px;
    }
  }
  .login-form .input-group {
    margin-top: 15px;
    font-size: 20px;
    color: #9e9e9e;
	  padding-bottom: 1px;
  }
  .login-form .form-control:focus {
      outline: none;
  }
  #show-password {
    float: right;
      vertical-align: bottom;
      text-align: center;
      margin-top: 7px;
      cursor: pointer;
  }
  .login-form .forgot-password {
    float: right;
  }
  .login-form .rememberme-container {
    margin-top: 15px;
    padding: 0;
  }
  .login-form .rememberme-container input {
    margin-left: 0;
  }
  .login-form .rememberme span {
    vertical-align: top;
  }

</style>

@section('title', 'Login')

@include('auth.header')
  <body class="hold-transition login-page">
    <div class="login-box" id="login-form">
          <form class="login-form" action="/postlogin" method="POST" autocomplete="off">
                @csrf
            @if(session('gagal'))
              <div class="alert alert-danger" role="alert">
                {{session('gagal')}}
              </div>
            @endif
            @if(session('success'))
              <div class="alert alert-success" role="alert">
                {{session('success')}}
              </div>
            @endif
                <div class="card mb-6">
                    <div class="card-body">
                        <div class="text-center mb-3">
                            <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                            <h5 class="mb-0">Login to your account</h5>
                            <span class="d-block text-muted">Enter your credentials below</span>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="icon-user text-muted"></span>
                              </div>
                            </div>
                            <input type="text" class="form-control" name="userid" placeholder="Username">
                        </div>
                        <span class="alert-message"><?php echo $errors->first('userid') ?></span>
                          <div class="input-group mb-3">
                            <div class="input-group-append">
                              <div class="input-group-text">
                              <span class="fas fa-lock"></span>
                              </div>
                            </div>
                            <input type="password"  id="password" class="form-control" name="password" placeholder="Password"/>
                            <div class="input-group-append">
                              <div class="input-group-text">
                              <span id="show-password" class="fa fa-eye"></span>
                              </div>
                            </div>
                        </div>
                        <div class="rememberme-container">
                          <input type="checkbox" name="rememberme" id="rememberme"/>
                          <label for="rememberme" class="rememberme"><span>Biarkan tetap masuk</span></label>
                          <a class="forgot-password" href="#">Lupa Password?</a>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-outline-primary">Sign in <i
                            class="icon-circle-right2 ml-2"></i></button>
                        </div>
                        <p class="mb-0">
                          <a href="{{ url('/register') }}" class="btn btn-block btn-outline-info">Register a new membership</a>
                        </p>
                    </div>
                </div>
          </form>
      </div>
  <script>
    $('#show-password').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('#password').attr('type', 'text');
        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');
      } else {
        $('#password').attr('type', 'password');
        $(this).removeClass('fa-eye-slash');
        $(this).addClass('fa-eye');
      }
    })
  </script>
</body>
</html>
