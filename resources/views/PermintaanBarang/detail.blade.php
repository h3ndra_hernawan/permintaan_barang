@extends('layouts.master')
@section('styles')
<!-- DataTables -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <link rel="stylesheet" href="{{url('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
  .alert-message {
    color: red;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
@endsection
@section('content')
<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="animated fadeIn">
                @if (session('success'))
                <div class="alert alert-success border-0 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ session('success') }}.
                </div>
                @endif

                @if (session('gagal'))
                <div class="alert alert-danger border-0 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ session('gagal') }}.
                </div>
                @endif
                @if(count($errors) > 0 )
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="modal_loding" id="modal_loding"><!-- Place at bottom of page --></div>
                <div class="alert alert-danger border-0 alert-dismissible" style="display: none" id="tes1">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <p id="text-tes1"></p>
                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="icon-plus"></i> Detail Permintaan Barang
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="table-responsive">
                                <div class="col-md-12">
                                    <table class="table table-hover table-striped table-bordered datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode Transaki</th>
                                                <th>Kode Barang</th>
                                                <th>Nama Barang</th>
                                                <th>Qty Pembelian</th>
                                                <th>Harga Satuan</th>
                                                <th>Total Bayar</th>
                                                <th>Tanggal Input</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $counter = 0
                                            @endphp
                                            @foreach ($header_transaksi as $key => $item)
                                            <tr>
                                                <td style="width: 5%">{{ $counter += 1 }}</td> 
                                                <td style="width: 10%">{{ $item->kode_transaksi }}</td> 
                                                <td style="width: 10%">{{ $item->kode_barang }}</td>          
                                                <td style="width: 10%">{{ $item->nama_barang }}</td>
                                                <td style="width: 10%">{{ $item->kuantiti }}</td>
                                                <td style="width: 10%">Rp.{{number_format($item->satuan, 0, ',', '.') }}</td>
                                                <td style="width: 10%">Rp.{{number_format($item->totbayar, 0, ',', '.') }}</td>
                                                <td style="width: 15%"><?php echo date('d-F-Y',strtotime($item->created_at)); ?></td>
                                            </tr> 
                                            @endforeach
                                        </tbody>
                                        <tr>
                                            <td colspan="8" align="right">
                                            <a href="/permintaan_barang" class="btn btn-primary"><i class="icon-reload"></i> Kembali</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
@endsection
@section('javascripts')
    @if( $massage = session('success'))
        <script type="text/javascript">
        Swal.fire({
            type: 'success',
            title:  "{{ $massage }}",
            text: 'success',
            timer: 4000,
        })
        </script>   
    @endif
    @if ( $massage = session('gagal'))
        <script type="text/javascript">
        Swal.fire({
            type: 'error',
            title:  "{{ $massage }}",
            text: 'Gagal',
            timer: 4000,
        })
    </script>    
    @endif
    <script type="module">
       
        $('#modal_loding').hide();
    </script>
    <script src="{{url('admin/dist/vendors/jquery/js/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" crossorigin="anonymous"></script>
    <script src="{{url('admin/dist/vendors/jquery-ui-dist/js/jquery-ui.js') }}"></script>
    <script src="{{url('admin/dist/vendors/select2/js/select2.min.js') }}"></script>
    <script src="{{url('admin/dist/vendors/popper.js/js/popper.min.js') }}"></script>
    <script src="{{url('admin/dist/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{url('admin/dist/vendors/pace-progress/js/pace.min.js') }}"></script>
    <script src="{{url('admin/dist/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
    <script src="{{url('admin/dist/vendors/@coreui/coreui-pro/js/coreui.min.js') }}"></script>
    <script src="{{url('admin/dist/vendors/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{url('admin/dist/vendors/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{url('admin/dist/js/datatables.js') }}"></script>
@endsection
