@extends('layouts.master')
@section('styles')
<!-- DataTables -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <link rel="stylesheet" href="{{url('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
  .alert-message {
    color: red;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
@endsection
@section('content')
<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="animated fadeIn">
                @if (session('success'))
                <div class="alert alert-success border-0 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ session('success') }}.
                </div>
                @endif

                @if (session('gagal'))
                <div class="alert alert-danger border-0 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ session('gagal') }}.
                </div>
                @endif
                @if(count($errors) > 0 )
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="modal_loding" id="modal_loding"><!-- Place at bottom of page --></div>
                <div class="alert alert-danger border-0 alert-dismissible" style="display: none" id="tes1">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <p id="text-tes1"></p>
                </div>
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary" id="btn-add">
                            <i class="icon-plus"></i> Tambah Permintaan Barang
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="table-responsive">
                                <div class="col-md-12">
                                    <table class="table table-hover table-striped table-bordered datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode Transaksi</th>
                                                <th>Metode Pembayaran</th>
                                                <th>Tanggal Transaksi</th>
                                                <th>Jumlah Transaksi</th>
                                                <th>Kasir Input</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                        $counter = 0
                                        @endphp
                                        @foreach ($footer_transaksi as $key => $item)
                                        <tr>
                                            <td style="width: 5%">{{ $counter += 1 }}</td>   
                                            <td style="width: 10%">{{ $item->kode_transaksi }}</td>          
                                            <td style="width: 10%">{{ $item->metodepembayaran }}</td>
                                            <td style="width: 15%"><?php echo date('d-F-Y',strtotime($item->tanggal_transaksi)); ?></td>
                                            <td style="width: 10%">Rp.{{number_format($item->jumlah, 0, ',', '.') }}</td>
                                            <td style="width: 10%">{{ $item->nama }}</td>
                                            <td style="width: 10%">
                                                <div style="white-space: nowrap;">
                                                @php
                                                    $Encrypted = base64_encode($item->kode_transaksi);
                                                @endphp
                                                    <a href="/data/permintaan_barang/{{  $Encrypted }}" class="btn btn-info btn-md"><i class="icon-check"></i> Detail</a>
                                                </div>
                                            </td>
                                        </tr> 
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <!-- Modal -->
                    <div class="modal fade cd-example-modal-xl" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl modal-primary">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><i class="icon-plus"></i> Tambah Tansaksi</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/permintaan_barang') }}" method="POST"  name="autoSumForm" enctype="multipart/form-data" onSubmit="return cekform()">
                                        @csrf
                                        <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <input class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" type="hidden" value="{{ auth()->user()->nama}}"required>
                                                        <div class="form-group">  
                                                            <label for="metodepembayaran">Metode Pembayaran<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('metodepembayaran') is-invalid @enderror" name="metodepembayaran" type="text" id="metodepembayaran" placeholder="Masukan metodepembayaran"required>
                                                            @if($errors->has('metodepembayaran'))
                                                                <span class="alert-message">{{$errors->first('metodepembayaran')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <input class="form-control @error('created_at') is-invalid @enderror" id="created_at" name="created_at" type="hidden" value="{{ date('Y-m-d H:i:s')}}"placeholder="Masukan created_at"required> 
                                                    <input class="form-control @error('kode_transaksi') is-invalid @enderror" id="kode_transaksi" name="kode_transaksi" type="hidden" value="{{ $new_date }}"required>
                                                    <div class="col-sm-4">
                                                      
                                                    </div>
                                                    <div class="col-sm-4">
                                                   
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">  
                                                            <label for="tanggal">Tanggal Transaksi<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('tanggal') is-invalid @enderror" name="tanggal" type="date" value="{{ date('Y-m-d') }}"placeholder="Masukan tanggal"required>
                                                            @if($errors->has('tanggal'))
                                                                <span class="alert-message">{{$errors->first('tanggal')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <td>
                                                <h4>Daftar Barang</h4>  
                                                </td> 
                                                <br>
                                                <div class="row">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-striped table-bordered datatable" >  
                                                            <thead>
                                                                <tr>
                                                                    <th>Kode Barang</th>
                                                                    <th>Kategori Barang</th>
                                                                    <th>Tersedia</th>
                                                                    <th>Qty Beli</th>
                                                                    <th>Harga Satuan</th>
                                                                    <th>Tottal Bayar</th>
                                                                    <th>Jumlah Bayar</th>
                                                                    <th>Kembalian</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead> 
                                                            <tbody id="dynamicTable">   
                                                                <tr hidden=true> 
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                        </tbody> 
                                                        <tr>
                                                            <td colspan="8" align="right">
                                                                <button type="button" name="add" id="add" class="btn btn-success"><i class="icon-plus"></i> Tambah</button>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" disabled="disabled" id="save_me" onclick="return confirm('Apakah Anda Yakin Data Yang Diinputkan Benar ? Jika Benar Klik Ok')"> Bayar</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div> 
</div> 
@endsection
  @section('javascripts')
  @if( $massage = session('success'))
        <script type="text/javascript">
        Swal.fire({
            type: 'success',
            title:  "{{ $massage }}",
            text: 'success',
            timer: 4000,
        })
        </script>   
    @endif
    @if ( $massage = session('gagal'))
        <script type="text/javascript">
        Swal.fire({
            type: 'error',
            title:  "{{ $massage }}",
            text: 'Gagal',
            timer: 4000,
        })
    </script>    
    @endif

    <script type="text/javascript">
        var status_detail = 0
        function hapusdetail(){
            status_detail -= 1;
            console.log('status' + status_detail)
            validate();
        }
        jQuery("#add").on("click", function () {
            validate();
        });
        function barang(x){  
            $('#lokasi'+ x).empty()
            $('#tersedia'+ x).empty()
            $('#satuan'+ x).empty()
            var kode_barang = document.getElementById('kode_barang'+ x);
            console.log('x', kode_barang) 
            $.ajax({
                url: "{{ url('/data/permintaan_barang') }}",
                method: "GET",
                data: {
                    id: kode_barang.value
                },
                success: function (res) {
                    res.forEach(element => {
                        document.getElementById('lokasi'+ x).value  = element.lokasi
                        document.getElementById('tersedia'+ x).value   = element.tersedia 
                        document.getElementById('satuan'+ x).value   = element.harga 
                    });
                }
            });     
        }
        function jumbayar1(x){
            let totbayar = document.getElementById('totbayar'+ x).value
            let jumbayar = document.getElementById('jumbayar'+ x).value
            if(totbayar < jumbayar)
              {
                alert('pembayaran Kurang')
                document.getElementById('jumbayar'+ x).value= null
              }
        }
        function jumbayar(x){
            let totbayar = document.getElementById('totbayar'+ x).value
            let jumbayar = document.getElementById('jumbayar'+ x).value
            if(totbayar === '' || totbayar === '0'){
                alert('Silahkan Isi qty Pembelian Terlebih Dahulu')
                document.getElementById('jumbayar'+ x).value= null
            }else{
               let kembalian =  jumbayar - totbayar
            
               document.getElementById('kembalian'+ x).value= kembalian
            }
        }
        function qty(x){
          let tersedia = document.getElementById('tersedia'+ x).value
          let kuantiti = document.getElementById('kuantiti'+ x).value
          let satuan = document.getElementById('satuan'+ x).value
          console.log('ter',kuantiti)
          if(tersedia ===''){
            alert('Silahkan Pilih Nama Barang Terlebih Dahulu')
            document.getElementById('kuantiti'+ x).value= null
          }else if(kuantiti > tersedia){
            alert('Kuantiti Yang Diinput Melebihi Jumlah Stok')
            document.getElementById('kuantiti'+ x).value= null
          }
             let tot_bayar =  kuantiti * satuan
             document.getElementById('totbayar'+ x).value= tot_bayar
        }
        function validate(x){
            jQuery("input[type='text']").each(function(){
                console.log("step 1");
                if (jQuery(this).val() != "" )
                {
                    console.log("step 2");
                if((jQuery("#totbayar"+ x).val()!="") && (jQuery("#lokasi"+ x).val()!="") && (jQuery("#tersedia"+ x).val()!="") && (jQuery("#kuantiti"+ x).val()!="") &&
                (jQuery("#satuan").val()!="") && (jQuery("#jumbayar"+ x).val()!="") &&
                (jQuery("#kembalian").val()!="") && (jQuery("#Departemen").val()!="") && 
                (status_detail > 0) && (jQuery("#metodepembayaran").val()!=""))
                {
                    console.log("step 3");
                    jQuery("#save_me").removeAttr("disabled"); 
                }
                else {
                    console.log("step 4");
                    jQuery("#save_me").attr("disabled", "disabled");
                }
                } 
            });
        }

        $('#tanggal').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
        });
        $(document).ready(function () {
            //   Modal
            $(document).on('click', '#btn-add', function(){  
                $('#modal_add').modal('show')

            })
            // Input Data Barang Header
            var i = 0;
       
            $("#add").click(function(e){
                ++i;  
                status_detail += i;
                console.log('status' + status_detail)

                validate();
                $("#dynamicTable").append(`
                        <tr>
                            <td>
                                <div class="form-group">  
                                <input class="form-control @error('created_at') is-invalid @enderror" id="created_at" name="permintaan['`+i+`'][created_at]" type="hidden" value="{{ date('Y-m-d H:i:s') }}"placeholder="Masukan created_at"required>                   
                                    <select class="form-control select form-input-select" onchange='barang(`+i+`)' onblur=validate(`+i+`) onclick=validate(`+i+`) data-fouc id="kode_barang`+ i +`"
                                        name="permintaan['`+i+`'][kode_barang]"required>
                                        <option value="" disabled selected>Pilih Master Barang</option>
                                        @foreach ($master_barang as $item)
                                        <option value="{{ $item->kode_barang }}" >{{ $item->kode_barang }} - {{ $item->nama_barang}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('kode_barang'))
                                        <span class="alert-message">{{$errors->first('kode_barang')}}</span>
                                    @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input class="form-control @error('lokasi') is-invalid @enderror" onblur=validate(`+i+`) id="lokasi`+i+`" name="permintaan['`+i+`'][lokasi]" type="text" placeholder="Kategori Barang" value="{{old('lokasi')}}"readonly>
                                    @if($errors->has('lokasi'))
                                        <span class="alert-message">{{$errors->first('lokasi')}}</span>
                                    @endif
                                </div>
                            </td> 
                            <td>
                                <div class="form-group">
                                <input class="form-control price" placeholder="Masukan Tersedia" onblur=validate(`+i+`) id="tersedia`+i+`" name="permintaan['`+i+`'][tersedia]"readonly>
                                @if($errors->has('tersedia'))
                                    <span class="alert-message">{{$errors->first('tersedia')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input type="number" class="form-control kuantiti" placeholder="Masukan Kuantiti" onblur=validate(`+i+`) onclick=validate(`+i+`) oninput='qty(`+i+`)' id="kuantiti`+i+`" name="permintaan['`+i+`'][kuantiti]">
                                @if($errors->has('kuantiti'))
                                    <span class="alert-message">{{$errors->first('kuantiti')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input type="text" class="form-control price" placeholder="Harga" onblur=validate(`+i+`) id="satuan`+i+`" name="permintaan['`+i+`'][satuan]"readonly>
                                @if($errors->has('satuan'))
                                    <span class="alert-message">{{$errors->first('satuan')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input class="form-control @error('totbayar') is-invalid @enderror" onblur=validate(`+i+`) id="totbayar`+i+`" name="permintaan['`+i+`'][totbayar]" type="text" placeholder="Total Bayar" value="{{old('totbayar')}}"readonly>
                                    @if($errors->has('totbayar'))
                                        <span class="alert-message">{{$errors->first('lokasi')}}</span>
                                    @endif
                                </div>
                            </td> 
                            <td>
                                <div class="form-group">
                                <input type="number" class="form-control price" placeholder="Masukan Jumlah Bayar" onfocusout='jumbayar1(`+i+`)' oninput='jumbayar(`+i+`)'  onblur=validate(`+i+`) onclick=validate(`+i+`) id="jumbayar`+i+`" name="permintaan['`+i+`'][jumbayar]"required>
                                @if($errors->has('jumbayar'))
                                    <span class="alert-message">{{$errors->first('jumbayar')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input type="text" class="form-control price" placeholder="Masukan Kembalian" onblur=validate(`+i+`) onclick=validate(`+i+`) id="kembalian`+i+`" name="permintaan['`+i+`'][kembalian]"readonly>
                                @if($errors->has('kembalian'))
                                    <span class="alert-message">{{$errors->first('kembalian')}}</span>
                                @endif
                                </div>
                            </td>
                            <td><button type="button" id="remove_detail" onclick='hapusdetail()' class="btn btn-danger remove-tr" >Remove</button></td>
                        </tr>
                    `);
            });
            $(document).on('click', '.remove-tr', function(){  
                    $(this).parents('tr').remove();
            }); 
        });
        $('#modal_loding').hide();
    </script>
<script src="{{url('admin/dist/vendors/jquery/js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" crossorigin="anonymous"></script>
<script src="{{url('admin/dist/vendors/jquery-ui-dist/js/jquery-ui.js') }}"></script>

<!-- DataTables -->
<script src="{{url('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{url('admin/plugins/jquery-ui/interactions.min.js') }}"></script>
<script src="{{url('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{url('admin/plugins/forms/selects/select2.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="{{url('admin/demo_pages/datatables_basic.js') }}"></script> -->
<script src="{{url('admin/demo_pages/form_select2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
@endsection
