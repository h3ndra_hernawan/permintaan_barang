@extends('layouts.master')
@section('styles')
<!-- DataTables -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <link rel="stylesheet" href="{{url('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
  .alert-message {
    color: red;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
@endsection
@section('content')

<div class="content-wrapper">
    <div class="content">
         <div class="container-fluid">
            <section class="content">
                    <div class="card card-primary card-outline">
                        <!-- <div class="card-body">
                            <p>Selamat Datang</p>
                            <strong><i class="fas fa-users"></i>  Halaman User</strong>
                        </div> -->
                        <!-- /.card-body -->
                        <!-- <hr> -->
                        <div class="card-header">
                            <a href="/dashboard" class="breadcrumb-item"><i class="fa fa-home"></i> Home</a>
                            <span class="breadcrumb-item active">Master Produk</span>
                        </div> <!-- /.card-body -->
                    </div>
            </section>
        </div>
    </div>    
    <div class="content">
        <section class="content">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Daftar Master Produk</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i></button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if (session('success'))
                                    <div class="alert alert-success border-0 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                        {{ session('success') }}.
                                    </div>
                                    @endif

                                    @if (session('message'))
                                    <div class="alert alert-danger border-0 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                        {{ session('message') }}.
                                    </div>
                                    @endif
                                    <a class="btn btn-success" href="javascript:void(0)" id="createNewUser"> <i class="fas fa-user-plus"></i> Tambah Data</a>          
                                    <br>
                                    <hr>
                                    <div class="panel-body">       
                                        <div style="overflow-x:auto;">
                                            <table class="table table-bordered table-hover datatable-basic">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th width="180px">Kode produk</th>
                                                        <th width="180px">Nama Produk</th>
                                                    <th width="180px">Merek Produk</th>
                                                    <th width="180px">Kategori Produk</th>
                                                        <th>Tahun </th>
                                                        <th>Stok Produk</th>
                                                        <th>Harga Produk</th>
                                                        <th>input</th>
                                                        <th width="180px">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>    
                                            </table>
                                        </div>   
                                                <div class="modal fade" id="ajaxModel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="modelHeading"></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="alert alert-danger print-error-msg" style="display:none">
                                                                <ul></ul>
                                                                </div>
                                                                <span aria-hidden="true">&times;</span>
                                                                <form id="ItemForm" name="ItemForm" class="form-horizontal">
                                                                <input type="hidden" name="id" id="id">
                                                                <input type="hidden" name="username" id="username" value="{{auth()->user()->nama}}">
                                                                <div class="form-group{{$errors->has('kode_produk') ? ' has-error' : ''}}">
                                                                        <label for="nama" class="col-lg-4 control-label">Kode Produk</label>
                                                                        <div class="col-lg-9">
                                                                            <input type="text" class="form-control" id="kode_produk" name="kode_produk" placeholder="Masukan Kode Produk" maxlength="50" required="" value="{{old('kode_produk')}}">
                                                                            @if($errors->has('kode_produk'))
                                                                            <span class="help-block">{{$errors->first('kode_produk')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                <div class="form-group{{$errors->has('nama_produk') ? ' has-error' : ''}}">
                                                                        <label for="nama_produk" class="col-lg-3 control-label">Nama Produk</label>
                                                                        <div class="col-lg-9">
                                                                            <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Masukan Nama Produk" maxlength="50" required="" value="{{old('nama_produk')}}">
                                                                            @if($errors->has('nama_produk'))
                                                                            <span class="help-block">{{$errors->first('nama_produk')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                <div class="form-group{{$errors->has('merek_produk') ? ' has-error' : ''}}">
                                                                        <label for="merek_produk" class="col-lg-3 control-label">Merek Produk</label>
                                                                        <div class="col-lg-9">
                                                                        <input type="text" class="form-control" id="merek_produk" name="merek_produk" placeholder="Masukan Merek Produk" maxlength="50" required="" value="{{old('merek_produk')}}">
                                                                            @if($errors->has('merek_produk'))
                                                                            <span class="help-block">{{$errors->first('merek_produk')}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                <div class="form-group{{$errors->has('kategori_produk') ? ' has-error' : ''}}">
                                                                        <label for="kategori_produk" class="col-lg-3 control-label">Kategori Produk</label>
                                                                        <div class="col-lg-9">
                                                                        <select class="form-control"  id="kategori_produk" name="kategori_produk" id="exampleFormControlSelect1">
                                                                            <option value="0" disabled selected>Pilih Kategori Produk</option>
                                                                            <option value="Makanan">Makanan</option>
                                                                            <option value="Minumam">Minumam</option>
                                                                            <option value="Elektronik">Elektronik</option>
                                                                            <option value="Sayuran">Novel</option>
                                                                            <option value="Buah-Buahan">Buah-Buahan</option>
                                                                            <option value="Pakaian">Pakaian</option>
                                                                        </select>
                                                                        @if($errors->has('kategori_produk'))
                                                                            <span class="help-block">{{$errors->first('kategori_produk')}}</span>
                                                                        @endif
                                                                        </div>
                                                                    </div>
                                                                <div class="form-group{{$errors->has('tahun_produk') ? ' has-error' : ''}}">
                                                                        <label for="tahun_produk" class="col-lg-3 control-label">Tahun Produk</label>
                                                                        <span class="error"><?php echo $errors->first('tahun_produk') ?></span>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                                <input type="number" id="tahun_produk" class="form-control " name="tahun_produk" placeholder="Masukan Tahun Produk">
                                                                            
                                                                                @if($errors->has('tahun_produk'))
                                                                            <span class="help-block">{{$errors->first('tahun_produk')}}</span>
                                                                        @endif
                                                                            </div>
                                                                        </div>    
                                                                    </div>
            
                                                                <div class="form-group{{$errors->has('stok') ? ' has-error' : ''}}">
                                                                        <label for="stok" class="col-lg-5 control-label">Stok</label>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                                <input type="number" id="stok" name="stok" class="form-control" placeholder="Masukan stok">
                                                                                <span id="stokError" class="alert-message"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group{{$errors->has('harga') ? ' has-error' : ''}}">
                                                                        <label for="harga" class="col-lg-5 control-label">Harga</label>
                                                                        <div class="col-lg-9">
                                                                            <div class="input-group mb-3">
                                                                                <input type="number" id="harga" name="harga" class="form-control" placeholder="Masukan harga">
                                                                                <span id="hargaError" class="alert-message"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                    <button type="button" class="btn btn-link" data-dismiss="modal">&times;Close</button>
                                                                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fas fa-database"></i> Save</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>    
                                        </div>    
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </section> 
                    </div>   
            </div>
    </div>              
</body>

  @endsection
  @section('javascripts')
<!-- DataTables -->
<script src="{{url('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{url('admin/plugins/jquery-ui/interactions.min.js') }}"></script>
<script src="{{url('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{url('admin/plugins/forms/selects/select2.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="{{url('admin/demo_pages/datatables_basic.js') }}"></script> -->
<script src="{{url('admin/demo_pages/form_select2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.datatable-basic').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('produk.index') }}",
        columns: [
            { "data": null,"sortable": false, 
                render: function (data, type, row, meta) {
                 return meta.row + meta.settings._iDisplayStart + 1;
                }  
            },
            {data: 'kode_produk', name: 'kode_produk'},
            {data: 'nama_produk', name: 'nama_produk'},
            {data: 'merek_produk', name: 'merek_produk'},
            {data: 'kategori_produk', name: 'kategori_produk'},
            {data: 'tahun_produk', name: 'tahun_produk'},
            {data: 'stok', name: 'stok'},
            {data: 'harga', name: 'harga'},
            {data: 'username', name: 'username'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewUser').click(function () {
        $('#saveBtn').val("create-user");
        $('#id').val('');
        $('#ItemForm').trigger("reset");
        $('#modelHeading').html("Tambah Data");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('produk.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Edit User");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#kode_produk').val(data.kode_produk);
          $('#nama_produk').val(data.nama_produk);
          $('#merek_produk').val(data.merek_produk);
          $('#kategori_produk').val(data.kategori_produk);
          $('#tahun_produk').val(data.tahun_produk);
          $('#stok').val(data.stok);
          $('#harga').val(data.harga);
          $('#username').val(data.username);
      })
   });
   $('body').on('click', '.showItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('produk.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Show User");
          $('#saveBtn').val("show-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#kode_produk').val(data.kode_produk);
          $('#nama_produk').val(data.nama_produk);
          $('#merek_produk').val(data.merek_produk);
          $('#kategori_produk').val(data.kategori_produk);
          $('#tahun_produk').val(data.tahun_produk);
          $('#stok').val(data.stok);
          $('#harga').val(data.harga);
          $('#username').val(data.username);
         
      })
      $('#saveBtn').modal('hide');
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
          data: $('#ItemForm').serialize(),
          url: "{{ route('produk.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
            if($.isEmptyObject(data.error)){
              $('#ItemForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              swal("Data produk Berhasil Di Simpan", data.message, "success");
              table.draw();
            }else{
              swal("Harap Cek inputan Data", data.message, "error");
                printErrorMsg(data.error);
            }
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    $('body').on('click', '.deleteItem', function () {

      var user_id = $(this).data("id");
      swal({
            title: "Delete?",
            text: "Yakin Mau Delete ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yakin",
            cancelButtonText: "Tidak!",
            reverseButtons: !0
        }).then(function (e) {
          if (e.value === true) {
            $.ajax({
                type: "DELETE",
                url: "{{ route('produk.store') }}"+'/'+user_id,
                success: function (data) {
                  $('#ItemForm').trigger("reset");
                  $('#ajaxModel').modal('hide');
                  swal("Data Produk Berhasil Di Delete!", data.message, "success");
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
              });
          } else {
                e.dismiss;
            }  
        }, function (dismiss) {
          return false;
      })
    });
     
  });
</script>
@endsection