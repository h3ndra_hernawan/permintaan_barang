<nav class="main-header navbar navbar-expand bg-green navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item bg-success">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      Wellcome, {{auth()->user()->nama}}
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item bg-success">
      <a href="{{ url('/logout') }}" class="nav-link" onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">
         <i class="fas fa-sign-out-alt"></i> <span>Logout</span></a>
         <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
      </li>
    </ul>
  </nav>