<aside class="main-sidebar sidebar-dark-success elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link bg-success">
        <img src="/global_assets/images/logo_light.png" style="opacity: .8">
      
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{auth()->user()->nama}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview {{ request()->is('dashboard') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link {{ request()->is('dashboard') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/dashboard') }}" class="nav-link {{ request()->is('/dashboard') ? 'active' : '' }}">
                    <ion-icon name="home-sharp"></ion-icon>
                  <p>Dashboard</p>
                </a>
              </li>
            </ul>
          </li>
           <!-- Master -->
           <li class="nav-header">Master</li>
           <li class="nav-item has-treeview {{ request()->is('register','produk','pelanggan') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link {{ request()->is('register','produk','pelanggan') ? 'active' : '' }}">
                <i class="nav-icon fas fa-users-cog"></i>
              <p>
                USER UTILITY
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                    <a href="{{ url('register') }}" class="nav-link {{ request()->is('register') ? 'active' : '' }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Daftar User</p>
                    </a>
              </li>
              </li>
              <li class="nav-item">
                    <a href="{{ url('produk') }}" class="nav-link {{ request()->is('produk') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Master Produk</p>
                    </a>
              </li>
              <li class="nav-item">
                    <a href="{{ url('pelanggan') }}" class="nav-link {{ request()->is('pelanggan') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Pelanggan</p>
                    </a>
              </li>
            </ul>
          </li>
               <!-- Projek User -->
        <li class="nav-header">Projek User</li>
          <li class="nav-item has-treeview {{ request()->is('projek_user') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link {{ request()->is('projek_user') ? 'active' : '' }}">
                <ion-icon name="stats-chart-outline" class="nav-icon"></ion-icon>
                <p>Projek User 
                    <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                    <a href="{{ url('projek_user') }}" class="nav-link {{ request()->is('projek_user') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                            <p>Projek User </p>
                    </a>
              </li>
            </ul>
        </li>
           <!-- Transaksi -->
        <li class="nav-header">Transaksi</li>
          <li class="nav-item has-treeview {{ request()->is('pesanan') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link {{ request()->is('pesanan') ? 'active' : '' }}">
                <ion-icon name="stats-chart-outline" class="nav-icon"></ion-icon>
                <p>TRANSAKSI
                    <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                    <a href="{{ url('transaksi') }}" class="nav-link {{ request()->is('transaksi') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                            <p>Transaksi</p>
                    </a>
              </li>
            </ul>
          </li>

          <!-- Report -->
          <li class="nav-header">Laporan</li>
          <li class="nav-item has-treeview {{ request()->is('laporan') ? 'menu-open' : '' }}">
            <a href="#" class="nav-link {{ request()->is('laporan') ? 'active' : '' }}">
                <ion-icon name="stats-chart-outline" class="nav-icon"></ion-icon>
                <p>Laporan
                    <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                    <a href="{{ url('laporan') }}" class="nav-link {{ request()->is('laporan') ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                            <p>Laporan</p>
                    </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>