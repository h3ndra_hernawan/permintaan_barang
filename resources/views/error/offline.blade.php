@extends('error.app')

@section('error-content')
<h1 class="error-title offline-title">Offline</h1>
<h5>Sorry, our website is temporary offline</h5>
@if (session('message'))
<div class="alert alert-danger border-0 alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
    {{ session('message') }}.
</div>
@endif
@endsection
