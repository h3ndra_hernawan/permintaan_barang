@extends('layouts.master')
@section('styles')
<!-- DataTables -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <link rel="stylesheet" href="{{url('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
  .alert-message {
    color: red;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
@endsection
@section('content')

<div class="content-wrapper">
    <div class="content">
        <section class="content">
                <div class="card card-primary card-outline">
                    <!-- <div class="card-body">
                        <p>Selamat Datang</p>
                        <strong><i class="fas fa-users"></i>  Halaman User</strong>
                    </div> -->
                    <!-- /.card-body -->
                    <!-- <hr> -->
                    <div class="card-header">
                        <a href="/dashboard" class="breadcrumb-item"><i class="fa fa-home"></i> Home</a>
                        <span class="breadcrumb-item active">User Utility</span>
                    </div> <!-- /.card-body -->
                </div>
        </section>
    </div>    
    <div class="content">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Daftar User</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (session('success'))
                            <div class="alert alert-success border-0 alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                {{ session('success') }}.
                            </div>
                            @endif

                            @if (session('message'))
                            <div class="alert alert-danger border-0 alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                {{ session('message') }}.
                            </div>
                            @endif
                            <a class="btn btn-success" href="javascript:void(0)" id="createNewUser"> <i class="fas fa-user-plus"></i> Tambah Data</a>          
                            <br>
                            <hr>
                            <div class="panel-body">       
                                <div style="overflow-x:auto;">
                                    <table class="table table-bordered table-hover datatable-basic">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%">No.</th>
                                                <th>Username</th>
                                                <th width="380px">Nama User</th>
                                                <th>Email</th>
                                                <th width="280px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>    
                                    </table>
                                </div>   
                                        <div class="modal fade" id="ajaxModel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="modelHeading"></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="alert alert-danger print-error-msg" style="display:none">
                                                          <ul></ul>
                                                        </div>
                                                        <span aria-hidden="true">&times;</span>
                                                        <form id="ItemForm" name="ItemForm" class="form-horizontal">
                                                        <input type="hidden" name="id" id="id">
                                                          <div class="form-group{{$errors->has('nama') ? ' has-error' : ''}}">
                                                                <label for="nama" class="col-lg-4 control-label">Nama Lengkap</label>
                                                                <div class="col-lg-9">
                                                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter Username" maxlength="50" required="" value="{{old('nama')}}">
                                                                    @if($errors->has('nama'))
                                                                      <span class="help-block">{{$errors->first('nama')}}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                          <div class="form-group{{$errors->has('userid') ? ' has-error' : ''}}">
                                                                <label for="userid" class="col-lg-3 control-label">Username</label>
                                                                <div class="col-lg-9">
                                                                    <input type="text" class="form-control" id="userid" name="userid" placeholder="Enter Username" maxlength="50" required="" value="{{old('userid')}}">
                                                                    @if($errors->has('userid'))
                                                                      <span class="help-block">{{$errors->first('userid')}}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                           <div class="form-group{{$errors->has('email') ? ' has-error' : ''}}">
                                                                <label for="email" class="col-lg-3 control-label">Email</label>
                                                                <div class="col-lg-9">
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter Name" maxlength="50" required="" value="{{old('email')}}">
                                                                    @if($errors->has('email'))
                                                                      <span class="help-block">{{$errors->first('email')}}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                           <div class="form-group{{$errors->has('Password') ? ' has-error' : ''}}">
                                                                <label for="password" class="col-lg-3 control-label">Password</label>
                                                                <span class="error"><?php echo $errors->first('password') ?></span>
                                                                <div class="col-lg-9">
                                                                    <div class="input-group mb-3">
                                                                        <input type="password" id="password" class="form-control " name="password" placeholder="Masukan Password">
                                                                        <div class="input-group-append">
                                                                            <div class="input-group-text">
                                                                            <span id="show-password" class="fa fa-eye"></span>
                                                                            </div>
                                                                        </div> 
                                                                        @if($errors->has('password'))
                                                                    <span class="help-block">{{$errors->first('password')}}</span>
                                                                  @endif
                                                                    </div>
                                                                </div>    
                                                            </div>
    
                                                           <div class="form-group{{$errors->has('confirmed') ? ' has-error' : ''}}">
                                                                <label for="password" class="col-lg-5 control-label">Confirm Password</label>
                                                                <div class="col-lg-9">
                                                                    <div class="input-group mb-3">
                                                                        <input type="password" id="confirmed" name="confirmed" class="form-control" placeholder="Masukan Confirm Password">
                                                                        <div class="input-group-append">
                                                                            <div class="input-group-text">
                                                                            <span id="confirmed-password" class="fa fa-eye"></span>
                                                                            </div>
                                                                        </div>
                                                                        <span id="confirmedError" class="alert-message"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                            <button type="button" class="btn btn-link" data-dismiss="modal">&times;Close</button>
                                                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fas fa-database"></i> Save</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>   
                                            </div>
                                        </div>    
                                </div>    
                            </div>
                        </div>
                    </div>    
                </div>
            </section> 
        </div>   
    </div>              
</body>

  @endsection
  @section('javascripts')
<!-- DataTables -->
<script src="{{url('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{url('admin/plugins/jquery-ui/interactions.min.js') }}"></script>
<script src="{{url('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{url('admin/plugins/forms/selects/select2.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="{{url('admin/demo_pages/datatables_basic.js') }}"></script> -->
<script src="{{url('admin/demo_pages/form_select2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

<script>

    $('#show-password').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('#password').attr('type', 'text');
        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');
      } else {
        $('#password').attr('type', 'password');
        $(this).removeClass('fa-eye-slash');
        $(this).addClass('fa-eye');
      }
    })
    $('#confirmed-password').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('#confirmed').attr('type', 'text');
        $(this).removeClass('fa-eye');
        $(this).addClass('fa-eye-slash');
      } else {
        $('#confirmed').attr('type', 'password');
        $(this).removeClass('fa-eye-slash');
        $(this).addClass('fa-eye');
      }
    })
  </script>
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.datatable-basic').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user.index') }}",
        columns: [
            { "data": null,"sortable": false, 
                render: function (data, type, row, meta) {
                 return meta.row + meta.settings._iDisplayStart + 1;
                }  
            },
            {data: 'userid', name: 'userid'},
            {data: 'nama', name: 'nama'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewUser').click(function () {
        $('#saveBtn').val("create-user");
        $('#id').val('');
        $('#ItemForm').trigger("reset");
        $('#modelHeading').html("Tambah Data");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('user.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Edit User");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#userid').val(data.userid);
          $('#nama').val(data.nama);
          $('#email').val(data.email);
          $('#password').val(data.password);
      })
   });
   $('body').on('click', '.showItem', function () {
      var user_id = $(this).data('id');
      $.get("{{ route('user.index') }}" +'/' + user_id +'/edit', function (data) {
          $('#modelHeading').html("Show User");
          $('#saveBtn').val("show-user");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#userid').val(data.userid);
          $('#nama').val(data.nama);
          $('#email').val(data.email);
          $('#password').val(data.password);
         
      })
      $('#saveBtn').modal('hide');
   });
    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
          data: $('#ItemForm').serialize(),
          url: "{{ route('user.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
            if($.isEmptyObject(data.error)){
              $('#ItemForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              swal("Data User Berhasil Di Simpan", data.message, "success");
              table.draw();
            }else{
              swal("Harap Cek inputan Data", data.message, "error");
                printErrorMsg(data.error);
            }
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    $('body').on('click', '.deleteItem', function () {

      var user_id = $(this).data("id");
      swal({
            title: "Delete?",
            text: "Yakin Mau Delete ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yakin",
            cancelButtonText: "Tidak!",
            reverseButtons: !0
        }).then(function (e) {
          if (e.value === true) {
            $.ajax({
                type: "DELETE",
                url: "{{ route('user.store') }}"+'/'+user_id,
                success: function (data) {
                  $('#ItemForm').trigger("reset");
                  $('#ajaxModel').modal('hide');
                  swal("Data User Berhasil Di Delete!", data.message, "success");
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
              });
          } else {
                e.dismiss;
            }  
        }, function (dismiss) {
          return false;
      })
    });
     
  });
</script>
@endsection