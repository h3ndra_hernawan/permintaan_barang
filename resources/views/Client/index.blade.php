@extends('layouts.master')
@section('styles')
<!-- DataTables -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <link rel="stylesheet" href="{{url('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <style>
  .alert-message {
    color: red;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
@endsection
@section('content')
<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
            <div class="animated fadeIn">
                @if (session('success'))
                <div class="alert alert-success border-0 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ session('success') }}.
                </div>
                @endif

                @if (session('gagal'))
                <div class="alert alert-danger border-0 alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    {{ session('gagal') }}.
                </div>
                @endif
                @if(count($errors) > 0 )
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="modal_loding" id="modal_loding"><!-- Place at bottom of page --></div>
                <div class="alert alert-danger border-0 alert-dismissible" style="display: none" id="tes1">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <p id="text-tes1"></p>
                </div>
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary" id="btn-add">
                            <i class="icon-plus"></i> Tambah Proyek Client
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="table-responsive">
                                <div class="col-md-12">
                                    <table class="table table-hover table-striped table-bordered datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode Projek</th>
                                                <th>User Id</th>
                                                <th>Nama Projek</th>
                                                <th>Tanggal</th>
                                                <th>Lokasi Projek</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                        $counter = 0
                                        @endphp
                                      
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <!-- Modal -->
                    <div class="modal fade cd-example-modal-xl" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl modal-primary">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><i class="icon-plus"></i> Tambah Projek</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @if(count($errors) > 0 )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <ul class="p-0 m-0" style="list-style: none;">
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="modal-body">
                                    <form action="{{ url('/permintaan_projek') }}" method="POST"  name="autoSumForm" enctype="multipart/form-data" onSubmit="return cekform()">
                                        @csrf
                                        <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <input class="form-control @error('user_id') is-invalid @enderror" id="userid" name="userid" type="hidden" value="{{ auth()->user()->userid}}"required>
                                                        <div class="form-group">  
                                                            <label for="nama_projek">Nama Proyek<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('nama_projek') is-invalid @enderror" name="nama_projek" type="text" id="nama_projek" placeholder="Masukan nama_projek"required>
                                                            @if($errors->has('nama_projek'))
                                                                <span class="alert-message">{{$errors->first('nama_projek')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <input class="form-control @error('kode_projek') is-invalid @enderror" id="kode_projek" name="kode_projek" type="hidden" value="{{ $new_date }}"required>
                                                    <div class="col-sm-4">
                                                    <div class="form-group">  
                                                            <label for="lokasi_projek">Lokasi Proyek<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('	lokasi_projek') is-invalid @enderror" name="lokasi_projek" type="text" id="	lokasi_projek" placeholder="Masukan 	lokasi_projek"required>
                                                            @if($errors->has('	lokasi_projek'))
                                                                <span class="alert-message">{{$errors->first('lokasi_projek')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                   
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">  
                                                            <label for="tanggal">Tanggal Transaksi<sup style="color: red">*</sup></label>
                                                            <input class="form-control @error('tanggal') is-invalid @enderror" name="tanggal" type="date" value="{{ date('Y-m-d') }}"placeholder="Masukan tanggal"required>
                                                            @if($errors->has('tanggal'))
                                                                <span class="alert-message">{{$errors->first('tanggal')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <td>
                                                <h4>Daftar Project</h4>  
                                                </td> 
                                                <br>
                                                <div class="row">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-striped table-bordered datatable" >  
                                                            <thead>
                                                                <tr>
                                                                    <th>Kode kode client/Nama</th>
                                                                    <th>Alamat</th>
                                                                    <th>No Tlp</th>
                                                                    <th>Deskripsi</th>
                                                                    <th>Estimasi Projek</th>
                                                                    <th>Mulai Projek</th>
                                                                    <th>Selesai Projek</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead> 
                                                            <tbody id="dynamicTable">   
                                                                <tr hidden=true> 
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                        </tbody> 
                                                        <tr>
                                                            <td colspan="8" align="right">
                                                                <button type="button" name="add" id="add" class="btn btn-success"><i class="icon-plus"></i> Tambah</button>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" disabled="disabled" id="save_me" onclick="return confirm('Apakah Anda Yakin Data Yang Diinputkan Benar ? Jika Benar Klik Ok')"> Bayar</button>
                                        </div>
                                    </form>    
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div> 
</div> 
@endsection
  @section('javascripts')
  @if( $massage = session('success'))
        <script type="text/javascript">
        Swal.fire({
            type: 'success',
            title:  "{{ $massage }}",
            text: 'success',
            timer: 4000,
        })
        </script>   
    @endif
    @if ( $massage = session('gagal'))
        <script type="text/javascript">
        Swal.fire({
            type: 'error',
            title:  "{{ $massage }}",
            text: 'Gagal',
            timer: 4000,
        })
    </script>    
    @endif

    <script type="text/javascript">
        var status_detail = 0
        function hapusdetail(){
            status_detail -= 1;
            console.log('status' + status_detail)
            validate();
        }
        jQuery("#add").on("click", function () {
            validate();
        });
        function barang(x){  
            $('#deskripsi_projek'+ x).empty()
            $('#alamat'+ x).empty()
            $('#no_tlp'+ x).empty()
            var kode_client = document.getElementById('kode_client'+ x);
            console.log('x',kode_client) 
            $.ajax({
                url: "{{ url('/data/permintaan_client') }}",
                method: "GET",
                data: {
                    id:kode_client.value
                },
                success: function (res) {
                    res.forEach(element => {
                        document.getElementById('deskripsi_projek'+ x).value  = element.deskripsi_projek
                        document.getElementById('alamat'+ x).value   = element.alamat 
                        document.getElementById('no_tlp'+ x).value   = element.no_tlp 
                    });
                }
            });     
        }
      
        function validate(x){
            jQuery("input[type='text']").each(function(){
                console.log("step 1");
                if (jQuery(this).val() != "" )
                {
                    console.log("step 2");
                if((jQuery("#totbayar"+ x).val()!="") && (jQuery("#lokasi"+ x).val()!="") && (jQuery("#tersedia"+ x).val()!="") && (jQuery("#kuantiti"+ x).val()!="") &&
                (jQuery("#satuan").val()!="") && (jQuery("#jumbayar"+ x).val()!="") &&
                (jQuery("#kembalian").val()!="") && (jQuery("#Departemen").val()!="") && 
                (status_detail > 0) && (jQuery("#metodepembayaran").val()!=""))
                {
                    console.log("step 3");
                    jQuery("#save_me").removeAttr("disabled"); 
                }
                else {
                    console.log("step 4");
                    jQuery("#save_me").attr("disabled", "disabled");
                }
                } 
            });
        }
        $(document).ready(function () {
            //   Modal
            $(document).on('click', '#btn-add', function(){  
                $('#modal_add').modal('show')

            })
            // Input Data Barang Header
            var i = 0;
       
            $("#add").click(function(e){
                ++i;  
                status_detail += i;
                console.log('status' + status_detail)

                validate();
                $("#dynamicTable").append(`
                        <tr>
                            <td>
                                <div class="form-group">  
                                    <select class="form-control select form-input-select" onchange='barang(`+i+`)' onblur=validate(`+i+`) onclick=validate(`+i+`) data-fouc id="kode_client`+ i +`"
                                        name="permintaan['`+i+`'][kode_client]"required>
                                        <option value="" disabled selected>Pilih Kode/nama</option>
                                        @foreach ($client as $item)
                                        <option value="{{ $item->kode_client }}" >{{ $item->kode_client }} - {{ $item->nama_client}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('kode_client'))
                                        <span class="alert-message">{{$errors->first('kode_client')}}</span>
                                    @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input class="form-control @error('alamat') is-invalid @enderror" onblur=validate(`+i+`) id="alamat`+i+`" name="permintaan['`+i+`'][alamat]" type="text" placeholder="Kategori Barang" value="{{old('alamat')}}"readonly>
                                    @if($errors->has('alamat'))
                                        <span class="alert-message">{{$errors->first('alamat')}}</span>
                                    @endif
                                </div>
                            </td> 
                            <td>
                                <div class="form-group">
                                <input class="form-control no_tlp" placeholder="Masukan no_tlp" onblur=validate(`+i+`) id="no_tlp`+i+`" name="permintaan['`+i+`'][no_tlp]"readonly>
                                @if($errors->has('no_tlp'))
                                    <span class="alert-message">{{$errors->first('no_tlp')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input type="text" class="form-control price" placeholder="deskripsi_projek" onblur=validate(`+i+`) id="deskripsi_projek`+i+`" name="permintaan['`+i+`'][deskripsi_projek]"readonly>
                                @if($errors->has('deskripsi_projek'))
                                    <span class="alert-message">{{$errors->first('deskripsi_projek')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input class="form-control estimasi_projek" placeholder="Masukan estimasi_projek" onblur=validate(`+i+`) id="estimasi_projek`+i+`" name="permintaan['`+i+`'][estimasi_projek]">
                                @if($errors->has('estimasi_projek'))
                                    <span class="alert-message">{{$errors->first('estimasi_projek')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input class="form-control" type="date" placeholder="Masukan mulai_projek" onblur=validate(`+i+`) id="mulai_projek`+i+`" name="permintaan['`+i+`'][mulai_projek]">
                                @if($errors->has('mulai_projek'))
                                    <span class="alert-message">{{$errors->first('mulai_projek')}}</span>
                                @endif
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                <input class="form-control" type="date" placeholder="Masukan selesai_projek" onblur=validate(`+i+`) id="selesai_projek`+i+`" name="permintaan['`+i+`'][selesai_projek]">
                                @if($errors->has('selesai_projek'))
                                    <span class="alert-message">{{$errors->first('selesai_projek')}}</span>
                                @endif
                                </div>
                            </td>
                            <td><button type="button" id="remove_detail" onclick='hapusdetail()' class="btn btn-danger remove-tr" >Remove</button></td>
                        </tr>
                    `);
            });
            $(document).on('click', '.remove-tr', function(){  
                    $(this).parents('tr').remove();
            }); 
        });
        $('#modal_loding').hide();
    </script>
<script src="{{url('admin/dist/vendors/jquery/js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" crossorigin="anonymous"></script>
<script src="{{url('admin/dist/vendors/jquery-ui-dist/js/jquery-ui.js') }}"></script>

<!-- DataTables -->
<script src="{{url('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{url('admin/plugins/jquery-ui/interactions.min.js') }}"></script>
<script src="{{url('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{url('admin/plugins/forms/selects/select2.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="{{url('admin/demo_pages/datatables_basic.js') }}"></script> -->
<script src="{{url('admin/demo_pages/form_select2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
@endsection
