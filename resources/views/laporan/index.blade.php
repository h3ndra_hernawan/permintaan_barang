@extends('layouts.master')
@section('styles')
<style>
  .alert-message {
    color: red;
  }
  body{
	font-family: sans-serif;
}
 
h1{
	text-align: center;
}
 
.input-tanggal{
	padding: 10px;
	font-size: 14pt;	
}

</style>
@endsection
@section('link')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.min.css" type="text/css" />
<!-- <script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.min.js"></script>
<script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.bundle.min.js"></script> -->
@endsection
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@if(Session::has('download.in.the.next.request'))
<meta http-equiv="refresh" content="5;url={{ Session::get('download.in.the.next.request') }}">
@endif
<div class="content-wrapper">
    <div class="content">
        <div class="container-fluid">
                <div class="animated fadeIn">
                    @if (session('success'))
                    <div class="alert alert-success border-0 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        {{ session('success') }}.
                    </div>
                    @endif

                    @if (session('gagal'))
                    <div class="alert alert-danger border-0 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        {{ session('gagal') }}.
                    </div>
                    @endif
                    <div class="alert alert-danger border-0 alert-dismissible" style="display: none" id="tes1">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        <p id="text-tes1"></p>
                    </div>
                    <div class="card">
                    <div class="card-header">
                        <i class="icon-docs"></i> Laporan Minimarket
                    </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="input-group">
                                            <input class="form-control input-form" type="date" id="tanggal_awal" autocomplete="off" name="tanggal_awal" placeholder="Tanggal awal" value="{{ date('Y-m-d') }}"required>
                                    </div> 
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                            <input class="form-control input-form" type="date" id="tanggal_akhir" autocomplete="off" name="tanggal_akhir" placeholder="Tanggal Akhir" value="{{ date('Y-m-d') }}"required>
                                    </div> 
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-md btn-outline-primary" id="search">CARI</button>
                                    <button class="btn btn-md btn-outline-success" id="excell"><i class="fa fa-file-excel-o"></i> EXCEL</button>
                                    <!-- <a href="/lap_buku_besar"class="btn btn-md btn-outline-info" id="pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a> -->
                                    <!-- <button class="btn btn-md btn-outline-info" id="pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</button> -->
                                </div>
                            </div>
                                <br>
                                <div class="row">
                                    <div class="table-responsive">
                                        <div class="col-md-12">
                                            <table class="table table-hover table-striped table-bordered datatable">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%">No</th>
                                                        <th style="width: 15%">Tanggal Transaksi</th>
                                                        <th style="width: 15%">Kode Transaksi</th>
                                                        <th style="width: 15%">Nama Barang</th>
                                                        <th style="width: 10%">Qty beli</th>
                                                        <th style="width: 10%">Harga Satuan</th>
                                                        <th style="width: 10%">Metode Pembayaran</th>
                                                        <th style="width: 10%">Total Bayar</th>
                                                        <th style="width: 10%"> User Input</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <h3><th colspan="1" style="text-align:right"></th></h3>
                                                        <th></th<th></th><th>JUMLAH :</th> <th></th> <th></th><th></th><th></th><th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </main>
        </div> 
    </div> 
</div> 
<form id="form-excell" action="/data/lap_buku_besar_excell" method="POST">
    @csrf
    <input type="hidden" name="tanggal_awal" id="tanggal-awal-excell" value="">
    <input type="hidden" name="tanggal_akhir" id="tanggal-akhir-excell" value="">
    <input type="hidden" name="action" id="action" value="lap_buku_besar_excell">
</form>
<form id="form-pdf" action="/data/lap_buku_besar_pdf" method="POST">
    @csrf
    <input type="hidden" name="tanggal_awal" id="tanggal-awal" value="">
    <input type="hidden" name="tanggal_akhir" id="tanggal-akhir" value="">
    <input type="hidden" name="action" id="action" value="lap_buku_besar_pdf">
</form>
@endsection
@section('javascripts')
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#search').on('click', function () {
                var tanggal_awal = $('#tanggal_awal').val()
                var tanggal_akhir = $('#tanggal_akhir').val()
                // var kec = $('#kec').val()
                if (Date.parse(tanggal_awal) > Date.parse(tanggal_akhir)) {
                    $('#tanggal_akhir').addClass('is-invalid')
                    $('#tanggal_awal').addClass('is-invalid')
                    $('#tes1').show()
                        $('#text-tes1').html('Tolong perhatikan tanggal awal dan tanggal akhir nya')
                } else {
                    $('#modal_loding').show();
                        $('#tanggal_awal').removeClass('is-invalid')
                        $('#tanggal_akhir').removeClass('is-invalid')
                        $('#tes1').hide()
                        var Dtable = $('.datatable').DataTable({
                            "footerCallback": function ( row, data, start, end, display ) {
                                var api = this.api(), data;
                    
                                    // Remove the formatting to get integer data for summation
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                            i.replace(/[\$,]/g, '')*1 :
                                            typeof i === 'number' ?
                                                i : 0;
                                };
                    
                                // Total over all pages
                            let transaksi = api
                                        .column( 4 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                let order = api
                                        .column( 5 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                let  total = api
                                        .column( 7 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );

                                    
                        
                                    // Total over this page
                                let pageTotal = api
                                        .column( 6, { page: 'current'} )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                        
                                    // Update footer
                                    $( api.column( 4).footer() ).html(
                                        rubah(transaksi)
                                
                                
                                        );
                                    $( api.column( 5).footer() ).html(
                                
                                        "Rp. "+rubah(order)
                            
                                    
                                    );
                                    $( api.column( 7 ).footer() ).html(
                                    
                                        "Rp. "+rubah(total)
                                    
                                    );
                            },
                        destroy: true,
                            
                    });
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/data/search-laporan-mini-marker',
                        type: "GET",
                        data: {
                            'tanggal_awal': tanggal_awal,
                            'tanggal_akhir': tanggal_akhir
                        },
                        success: function (result) {
                            console.log(result);
                            if (result=='') {
                                $('#tes1').show()
                                $('#text-tes1').html('Data Tidak Ditemukan')
                                Dtable.clear().draw();
                                $('#modal_loding').hide();
                            } else {
                                Dtable.clear().draw();
                                var no = 0; 
                                result.forEach(element => {
                                    Dtable.row.add([
                                        no +=1,
                                        element.tanggal_transaksi,
                                        element.kode_transaksi,
                                        element.nama_barang,
                                        element.kuantiti,
                                        element.satuan,
                                        element.metodepembayaran,
                                        element.totbayar,
                                        element.userid
                                    ]);
                                });
                                Dtable.columns.adjust().draw();
                            }                    
                        }
                    })
                    
                
                    $('#excell').on('click', function () {
                        var input = document.getElementsByClassName('input-form')
                        var valid = true

                        for (let index = 0; index < input.length; index++) {
                            if (input[index].value == '') {
                                valid = false
                                input[index].classList.add('is-invalid')
                            }
                        }
                        // if ($('#kec').val() == null) {
                        //     valid = false
                        // }
                        if (valid == true) {
                            $('#tanggal-awal-excell').val($('#tanggal_awal').val())
                            $('#tanggal-akhir-excell').val($('#tanggal_akhir').val())
                            $('#form-excell').submit();
                        }

                    })
                    $('#pdf').on('click', function () {
                        var input = document.getElementsByClassName('input-form')
                        var valid = true

                        for (let index = 0; index < input.length; index++) {
                            if (input[index].value == '') {
                                valid = false
                                input[index].classList.add('is-invalid')
                            }
                        }
                        // if ($('#kec').val() == null) {
                        //     valid = false
                        // }
                        if (valid == true) {
                            $('#tanggal-awal').val($('#tanggal_awal').val())
                            $('#tanggal-akhir').val($('#tanggal_akhir').val())
                            $('#form-pdf').submit();
                        }

                    })
                }
            
            })
          
            function rubah(angka) {
                        var simbol ='';
                        if (angka.toString().substring(0,1) == '-'){
                            simbol='-';
                            angka = angka.toString().substring(1);
                        }
                        var reverse = angka.toString().split('').reverse().join(''),
                            ribuan = reverse.match(/\d{1,3}/g);
                        ribuan = ribuan.join('.').split('').reverse().join('');
                        return simbol.concat(ribuan);
                    }
        });
    </script>
    <script src="{{url('admin/dist/vendors/jquery/js/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" crossorigin="anonymous"></script>
<script src="{{url('admin/dist/vendors/jquery-ui-dist/js/jquery-ui.js') }}"></script>

<!-- DataTables -->
<script src="{{url('admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{url('admin/plugins/jquery-ui/interactions.min.js') }}"></script>
<script src="{{url('admin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{url('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{url('admin/plugins/forms/selects/select2.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<!-- <script src="{{url('admin/demo_pages/datatables_basic.js') }}"></script> -->
<script src="{{url('admin/demo_pages/form_select2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
@endsection
