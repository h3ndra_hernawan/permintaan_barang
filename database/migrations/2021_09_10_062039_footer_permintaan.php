<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FooterPermintaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footer_transaksi', function (Blueprint $table) {
            $table->string('kode_transaksi')->unique();
            $table->string('nik',100);
            $table->string('metodepembayaran',100);
            $table->double('jumlah');
            $table->timestamp('tanggal_transaksi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footer_transaksi');
    }
}
